\chapter{Protocollo IPv4}
\label{cap:protocollo_ipv4}
L'\textbf{Internet Protocol} (IP) è il livello di rete di TCP/IP:
\begin{itemize}
 \item è un servizio non connesso: non c'è segnalazione di rete o utente;
\item è un protocollo di tipo datagram: basato su pacchetti.
\end{itemize}

\paragraph{Funzionalità}
\begin{itemize}
 \item frammentazione e riassemblaggio dei pacchetti per il trasporto sul livello 2;
\item gestione di indirizzi a 32 bit (IPv4);
\item configurazione delle classi di servizio: i progettisti di IP avevano previsto una differenziazione del traffico per priorità, ma oggi le classi di servizio non sono sfruttate.
\end{itemize}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.25\linewidth]{pic/09/Router_(standard_notation).png}
\end{figure}

Gli apparati di interconnessione di livello 3 sono chiamati \textbf{router}:
\begin{itemize}
 \item a differenza degli switch evitano l'intasamento della forwarding table;
\item effettuano solo la frammentazione, mentre il riassemblaggio è delegato alla destinazione:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\linewidth]{pic/09/Router-level_fragmentation.png}
\end{figure}
\end{itemize}

\section{Pacchetti IP}
\subsection{Intestazione}
Il formato dell'intestazione di un pacchetto IP è composto da vari campi:
\begin{itemize}
 \item versione: IPv4 o IPv6;
\item tipo di servizio (Type of Service [ToS]): per le classi di servizio;
\item lunghezza totale del pacchetto prima della deframmentazione;
\item identificatore: tutti i frammenti di uno stesso pacchetto condividono lo stesso identificatore;
\item flag: indica se il pacchetto corrente è l'ultimo frammento;
\item offset di frammento: per l'ordinamento del pacchetto nella sequenza;
\item tempo di vita (Time To Live [TTL]): è un contatore di \textbf{hop}, cioè è un numero che viene decrementato di un'unità a ogni passaggio dentro un router $\Rightarrow$ quando arriva a 0 il pacchetto viene scartato per evitare che pacchetti vaghino all'infinito;
\item protocollo: identifica il protocollo di livello 4 del pacchetto payload (TCP, UDP, ICMP\dots);
\item checksum: protegge solo l'intestazione;
\item indirizzi IP della sorgente e della destinazione;
\item alcune opzioni oggi abbandonate.
\end{itemize}

\subsection{Indirizzi}
\begin{figure}
	\centering
	\includegraphics[width=0.5\linewidth]{pic/09/Classi_di_indirizzi_IPv4.png}
	\caption{Classi di indirizzi IP.}
\end{figure}

\noindent
Ogni indirizzo IP è ampio 4 byte:
\begin{itemize}
 \item campo rete: interfacce vicine sono aggregate in un \textbf{prefisso di rete} comune che occupa i bit alti;
\item campo host: identifica la macchina specifica.
\end{itemize}

Un indirizzo IP è umanamente rappresentabile suddividendo i 32 bit in gruppi da 8, convertendo ogni gruppo in decimale e separandoli con dei puntini $\Rightarrow$ ogni numero è compreso tra 0 e 255.

Non tutti gli indirizzi possibili sono utilizzabili:
\begin{itemize}
 \item indirizzi con tutti 0 nel campo host identificano l'intera rete e non possono essere utilizzati per un singolo host;
\item indirizzi con tutti 1 nel campo host identificano il broadcast di tipo \textbf{directed} a tutti gli host nella rete;
\item indirizzi con tutti 1 identificano il broadcast di tipo \textbf{limited} a tutti gli host della rete locale;
\item indirizzi che iniziano con 127 identificano la macchina stessa e bloccano il forwarding di traffico (\textbf{loopback});
\item indirizzi privati: gli indirizzi pubblici devono essere univoci in tutta Internet, gli indirizzi privati sono gestiti all'interno della rete privata che dall'esterno viene vista con un unico indirizzo IP (NAT\footnote{Si veda il capitolo~\ref{cap:nat}.}):
\begin{itemize}
 \item 1 rete privata di classe C, 2\textsuperscript{24} indirizzi: tra 10.0.0.0 e 10.255.255.255;
\item 15 reti private di classe B, 16 indirizzi: da 172.16.0.0 a 172.31.255.255;
\item 256 reti private di classe C, 256 indirizzi: da 192.168.0.0 a 192.168.255.255.
\end{itemize}
\end{itemize}

Gli indirizzi IP sono assegnati alle interfacce $\Rightarrow$ un router non ha un unico indirizzo, ma ne ha uno per ogni interfaccia.
\FloatBarrier

\section{Reti fisiche e reti logiche}
Una \textbf{rete fisica} è un insieme di host connessi in una rete senza router $\Rightarrow$ il livello fisico non viene interrotto da un dispositivo di livello 3.

Una \textbf{rete logica} (Logical IP Subnet [LIS]) è un insieme di host con lo stesso prefisso di rete, e include la rete fisica e il router. Ogni rete fisica corrisponde a livello 3 a una rete logica.

\begin{figure}
	\centering
	\includegraphics[width=0.85\linewidth]{pic/09/Routing_between_LISes.png}
\end{figure}

La comunicazione tra due macchine appartenenti a una stessa rete fisica è basata sugli indirizzi MAC dei due host (CSMA/CD). In realtà il livello applicazione non sa che l'host di destinazione si trova nella stessa rete fisica, e fornisce l'indirizzo IP $\Rightarrow$ \textbf{consegna diretta} al livello 2: è il livello 3 che deve capire che la destinazione è nella rete fisica e far partire i meccanismi di livello 2 senza passare dal router.

La consegna tra reti logiche differenti è affidata al router (\textbf{consegna indiretta}). Il livello 2 è comunque usato per le consegne dirette tra la sorgente e il router, e tra il router e la destinazione. L'host deve quindi conoscere almeno un \textbf{default gateway}, che è il router per uscire dalla rete logica.

Esiste la possibilità di emulare due reti logiche in una stessa rete fisica: una stessa interfaccia è vista da un host con un certo indirizzo IP e da un altro host con un altro indirizzo IP, e la comunicazione tra i due host passa sempre attraverso il router. In realtà esistono dei meccanismi che permettono di ottimizzare il percorso (routing table, ARP table): dopo la prima comunicazione, il router istruisce il primo host affinché la volta dopo svolga la consegna diretta a livello 2.
\FloatBarrier

\subsection{Subnetting}
Il \textbf{subnetting} consente partendo da una rete di una certa classe di creare una rete a classe più piccola: il prefisso di rete si espande e una parte del campo host diventa il campo subnet.

La \textbf{subnet mask} è una sequenza di bit:
\begin{itemize}
 \item i byte 255 identificano il prefisso di rete, compreso il campo subnet;
\item i byte 0 identificano il campo host ridotto.
\end{itemize}

Queste reti a classe più piccola sono delle reti logiche distinte $\Rightarrow$ il subnetting si può usare per emulare più reti logiche in una stessa rete a classe maggiore.

\subsection{Indirizzamento classless (CIDR)}
\begin{figure}
	\centering
	\includegraphics[width=0.66\linewidth]{pic/09/Esempio_netmask_IPv4.png}
\end{figure}

\noindent
La \textbf{netmask} permette di estendere il prefisso di rete a un qualsiasi bit dell'indirizzo:
\begin{itemize}
 \item i bit 1 identificano il prefisso di rete;
\item i bit 0 identificano il campo host.
\end{itemize}

In questo modo più reti piccole possono venire aggregate in un unico router $\Rightarrow$ si definiscono delle gerarchie di indirizzamento.

Non sono ammesse le netmask 255.255.255.255 (/32) e 255.255.255.254 (/31) perché l'indirizzo con tutti 0 è riservato alla rete stessa, e quello con tutti 1 al traffico broadcast.
\FloatBarrier

\subsection{Routing}
Dalla netmask il router può capire se la destinazione si trova nella stessa rete del mittente, confrontando i risultati di due operazioni AND bit a bit:
\begin{itemize}
 \item il primo AND bit a bit è tra l'indirizzo del mittente e la netmask del mittente;
\item il secondo AND bit a bit è tra l'indirizzo del destinatario e la netmask del mittente.
\end{itemize}

Quando al router arriva un datagramma:
\begin{itemize}
 \item se la destinazione coincide con il router stesso: elaborazione locale (per configurare il router);
\item se la destinazione è in uno degli address range del router: ARP,\footnote{Si veda il capitolo~\ref{cap:arp}} invio diretto all'indirizzo MAC di destinazione, ecc. (se l'indirizzo di destinazione appartiene a più address range, viene preferito quello con la netmask più lunga (Longest Prefix Matching));
\item se la destinazione è fuori da tutti gli address range del router: consulta la routing table, quindi invia il datagramma al ``prossimo hop'' indicato nella routing table (o sulla default route).
\end{itemize}

\subsubsection{Tabelle di routing}
Una \textbf{tabella di routing} può contenere:
\begin{itemize}
 \item entry dirette: sono le coppie indirizzo di rete (con 0 finali) + netmask associate a ogni interfaccia del router;
\item entry indirette: sono gli indirizzi IP delle interfacce degli altri router (next hop) a cui fare le consegne dirette:
\begin{itemize}
 \item statiche: sono configurate manualmente dal gestore;
\item dinamiche: sono determinate in modo automatico.
\end{itemize}
\end{itemize}

Si scelgono sempre le route a minor costo.

\begin{figure}[H]
	\centering
	\includegraphics[width=\linewidth]{pic/09/Esempio_tabella_di_routing.png}
	\caption{Esempio di routing table.}
\end{figure}

Più address range con uguali bit alti si posso aggregare, mettendo degli zeri al posto dei bit che differiscono:
\begin{table}[H]
 \centering
 \caption{A sinistra la routing table non aggregata, a destra la routing table aggregata.}
\centerline{
%  \begin{tabular}{|c|c|c|}
%  \hline
%  \textbf{Destinazione} & \textbf{Netmask} & \textbf{Next hop}\\
%  \hline
%  190.3.1.0 & 255.255.255.0 & 190.3.3.2\\
%  \hline
%  190.3.9.0 & 255.255.255.252 & 190.3.3.2\\
%  \hline
%  190.3.\textbf{9}.\textbf{4} & 255.255.\textbf{255}.\textbf{252} & 190.3.6.8\\
%  \hline
%  190.3.\textbf{4}.\textbf{0} & 255.255.\textbf{255}.\textbf{0} & 190.3.6.8\\
%  \hline
%  150.1.0.0 & 255.255.0.0 & 190.3.6.8\\
%  \hline
%  \end{tabular}
%  \ \hspace{.05\textwidth} \
%  \begin{tabular}{|c|c|c|}
%  \hline
%  \textbf{Destinazione} & \textbf{Netmask} & \textbf{Next hop}\\
%  \hline
%  190.3.1.0 & 255.255.255.0 & 190.3.3.2\\
%  \hline
%  190.3.9.0 & 255.255.255.252 & 190.3.3.2\\
%  \hline
%  \multirow{2}{*}{190.3.\textbf{0}.\textbf{0}} & \multirow{2}{*}{255.255.\textbf{0}.\textbf{0}} & \multirow{2}{*}{190.3.6.8}\\
%  \hline
%  150.1.0.0 & 255.255.0.0 & 190.3.6.8\\
%  \hline
%  \end{tabular}
 \begin{tabular}{|c|c|c|c|c|c|c|}
 \cline{1-3} \cline{5-7}
 \textbf{Destinazione} & \textbf{Netmask} & \textbf{Next hop} &  & \textbf{Destinazione} & \textbf{Netmask} & \textbf{Next hop}\\
 \cline{1-3} \cline{5-7}
 190.3.1.0 & 255.255.255.0 & 190.3.3.2 & & 190.3.1.0 & 255.255.255.0 & 190.3.3.2\\
 \cline{1-3} \cline{5-7}
 190.3.9.0 & 255.255.255.252 & 190.3.3.2 & & 190.3.9.0 & 255.255.255.252 & 190.3.3.2\\
 \cline{1-3} \cline{5-7}
 190.3.\textbf{9}.\textbf{4} & 255.255.\textbf{255}.\textbf{252} & 190.3.6.8 & & \multirow{2}{*}{190.3.\textbf{0}.\textbf{0}} & \multirow{2}{*}{255.255.\textbf{0}.\textbf{0}} & \multirow{2}{*}{190.3.6.8}\\
 \cline{1-3}
 190.3.\textbf{4}.\textbf{0} & 255.255.\textbf{255}.\textbf{0} & 190.3.6.8 & & & & \\
 \cline{1-3} \cline{5-7}
 150.1.0.0 & 255.255.0.0 & 190.3.6.8 & & 150.1.0.0 & 255.255.0.0 & 190.3.6.8\\
 \cline{1-3} \cline{5-7}
 \end{tabular}
}
\end{table}

Il default gateway ha sempre netmask 0.0.0.0, perché viene scelto solo se non ci sono altri address range che risultano validi dai due AND bit a bit.