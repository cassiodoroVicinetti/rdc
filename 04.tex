\chapter{Livello 1 OSI}
\label{cap:livello_1_osi}
Il \textbf{livello fisico} (physical) è lo strato deputato al trasporto fisico dei bit di informazione tra un sistema e l'altro. Un livello fisico è definito in base a codifiche di linea, connettori, livelli di tensione, ecc.

\section{Mezzi di trasmissione}
\subsection{Mezzi elettrici (cavi di rame)}
Il mezzo ottimale è caratterizzato da:
\begin{itemize}
 \item buone caratteristiche elettriche, cioè resistenza, capacità parassite e impedenza basse;
 \item buona resistenza alla trazione;
 \item flessibilità (le fibre ottiche non sono flessibili).
\end{itemize}

Le caratteristiche dei mezzi elettrici dipendono da:
\begin{itemize}
 \item geometria;
 \item numero di conduttori e distanza reciproca;
 \item tipo di isolante;
 \item tipo di schermatura.
\end{itemize}

\paragraph{Parametri di merito}
\begin{itemize}
 \begin{minipage}[t]{\linewidth}
 \item
 \begin{wrapfigure}[9]{r}{.45\textwidth}
\fbox{\begin{minipage}{\dimexpr\linewidth-2\fboxrule-2\fboxsep}
\textbf{dB}
\[
 {\left( x \right)}_{\text{dB}} = 10 \log_{10}{\left( \frac{x}{x_{\text{rif}}} \right)}
\]
\[
 {\left( P \right)}_{\text{dBm}} = 10 \log_{10} {\left( \frac{P \, \left[ \text{W} \right]}{1 \, \text{mW}} \right)}
\]
\[
 {\left( P_{\text{RX}} \right)}_{\text{dBm}} = { \left( P_{\text{TX}} \right) }_{\text{dBm}} - {\left( \text{perdita} \right)}_{\text{dB}}
\]
\end{minipage}}
\end{wrapfigure}
 impedenza;
 \item velocità di propagazione del segnale: 0,5$c$ - 0,7$c$ per cavi di rame, 0,6$c$ per fibre ottiche;
 \item \textbf{attenuazione}, cioè la perdita di potenza del segnale trasmesso con l'aumentare della distanza: cresce linearmente \ul{in dB} con la distanza;
 \item \textbf{diafonia} (o cross-talk), cioè la misura del disturbo indotto da un cavo vicino: vicino alla sorgente di trasmissione è poco disturbato, ma cresce con la distanza fino a stabilizzarsi.
 \end{minipage}
\end{itemize}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.4\linewidth]{pic/04/Parametri_di_merito_cavi_elettrici.png}
\end{figure}

\subsubsection{Doppino}
Il \textbf{doppino} è costituito da due fili di rame intrecciati per ridurre l'effetto delle interferenze dall'esterno. Ha un costo molto basso.

La versione UTP del doppino non presenta schermature. Vi sono 7 categorie, di qualità e prezzo crescenti:
\begin{enumerate}
 \item telefonia analogica
 \item telefonia ISDN
 \item reti locali fino a 10 Mb/s
 \item reti locali fino a 16 Mb/s
 \item reti locali fino a 100 Mb/s: il connettore si attacca a 4 doppini
 \item[5e.] reti locali fino a 1 Gb/s: il connettore si attacca a 4 doppini
 \item reti locali fino a 1 Gb/s: in fase di standardizzazione
\end{enumerate}

\subsubsection{Cavo coassiale}
In un \textbf{cavo coassiale}, il filo centrale è circondato da una maglia che fa da massa e da un isolante per ridurre le interferenze dei disturbi esterni (gabbia di Faraday). Grazie al maggiore spessore del conduttore l'impedenza è molto bassa, ma non è molto flessibile. Consente velocità di trasmissione dell'ordine di centinaia di Mb/s, ma è costoso.

\subsection{Mezzi ottici (fibre ottiche)}
La \textbf{fibra ottica} è costituita da un filo molto sottile di vetro, con dei rivestimenti di protezione.

\paragraph{Vantaggi}
\begin{itemize}
 \item totale immunità dai disturbi elettromagnetici, anche se la probabilità di errore non è perfettamente nulla;
 \item alta capacità di trasmissione (fino a decine di Terabit/s);
 \item il segnale trasmesso viaggia ``rimbalzando'' sulle pareti e viene ricevuto con un'attenuazione molto bassa $\Rightarrow$ i cavi possono essere molto lunghi;
 \item dimensioni ridotte e costi contenuti.
\end{itemize}

\paragraph{Svantaggi}
\begin{itemize}
 \item bassa flessibilità: si spezza facilmente;
 \item difficili da collegare tra loro (giunzioni) e con connettori: serve il microscopio $\Rightarrow$ costo elevato;
 \item la comunicazione è unidirezionale, e le connessioni possono essere solo punto-punto.
\end{itemize}

\subsubsection{Finestre di lavoro}
\begin{figure}
	\centering
	\includegraphics[width=0.33\linewidth]{pic/04/Finestre_di_lavoro_fibre_ottiche.png}
\end{figure}

\noindent
Ci sono tre tipi di laser:
\begin{enumerate}
 \item lunghezza d'onda 0,8 \textmu m: costo molto basso ma attenuazione alta;
 \item lunghezza d'onda 1,3 \textmu m: si trova subito prima della frequenza a perdita massima (1,4 \textmu m);
 \item lunghezza d'onda 1,55 \textmu m: perdita minima, ma costo massimo.
\end{enumerate}
\FloatBarrier

\subsection{Mezzi radio (onde radio)}
Una \textbf{trasmissione via radio} può subire delle perdite in ampiezza dovute a:
\begin{itemize}
 \item presenza di ostacoli nell'ambiente:
\begin{itemize}
\item fading (variazione veloce): il segnale si riflette sugli oggetti dell'ambiente che si muovono dinamicamente, e si creano delle interferenze costruttive o distruttive (perché in controfase);
\item shadowing (variazione lenta): la gran parte del segnale viene bloccata da un oggetto vicino al trasmettitore;
\end{itemize}
\item interferenze co-canale: interferenze con altri segnali;
\item attenuazione: a differenza dei cavi elettrici, il segnale si attenua con il quadrato della distanza.
\end{itemize}

\section{Reti di trasporto}
La \textbf{rete di trasporto} comprende gli apparati e i mezzi di trasmissione, appartenenti a uno o più gestori, che collegano due nodi di accesso.

\subsection{PDH}
Il \textbf{Plesiochronous Digital Hierarchy} (PDH) è un vecchio standard progettato per il trasferimento di canali vocali numerici a 64 Kb/s (PCM) nelle reti telefoniche. Le reti PDH sono reti a circuito digitale basate sul TDM:\footnote{Si veda la sezione~\ref{sez:tdm}.} i dati non viaggiano con lo store and forward, ma in flussi organizzati secondo una trama temporale. Il sistema è detto ``plesiocrono'' perché occorre una stretta sincronizzazione tra trasmettitore e ricevitore. Esistono diversi standard nel mondo.

Vi è una \textbf{gerarchia TDM} se i flussi di dati vengono aggregati in flussi di livello via via superiore secondo delle regole stabilite; più si sale di livello gerarchico, più la velocità di trasferimento è elevata:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\linewidth]{pic/04/PDH_-_TDM_hierarchy}
\end{figure}

Il PCM ha una frequenza di campionamento di 8 kHz, cioè 8000 campioni al secondo, e ogni campione è pari a 8 bit $\Rightarrow$ al livello T0 arriva un campione ogni 1 / 8000 = 125 \textmu s. La durata delle trame nei livelli successivi deve continuare a essere uguale a 125 \textmu s perché il flusso di dati dev'essere continuo. Siccome vengono aggregati 24 canali di livello T0 più un bit di segnalazione, al livello T1 in un secondo vengono aggregati 8000 \texttimes\ (8 \texttimes\ 24 + 1) = 1,544 Mbit/s.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\linewidth]{pic/04/PDH_-_T_hierarchy.png}
\end{figure}

Per isolare una singola telefonata all'interno di un flusso bisogna demultiplare un livello per volta, utilizzando alcuni bit di segnalazione per la sincronizzazione (ogni apparato ha un proprio clock).

\subsection{SONET/SDH}
Nel \textbf{Synchronous Digital Hierarchy} (SDH) esiste un clock unico per l'intero sistema, grazie a una rete di sincronizzazione o tramite il GPS $\Rightarrow$ ogni telefonata si trova in una definita posizione di bit ed è così facilmente rintracciabile. L'SDH è l'equivalente europeo dello standard internazionale SONET. La topologia è spesso ad anello per garantire l'affidabilità: un blocco nella rete deve essere recuperato istantaneamente, anche se a costo maggiore.

L'SDH funziona solo su fibra ottica perché le velocità sono incompatibili con i cavi di rame.

La multiplazione dei flussi non è banale come nel PDH (un canale a fianco dell'altro), ma è progettata per ottimizzare l'elaborazione via hardware. Ogni trama temporale include nella PCI:
\begin{itemize}
 \item informazioni di sincronizzazione per distinguere l'inizio della trama;
 \item canali vocali di servizio;
 \item gestione guasti/errori.
\end{itemize}

Anche nell'SDH ogni trama deve durare 125 \textmu s in tutti i livelli.

\section{Reti di accesso}
La \textbf{rete di accesso} (o local loop) comprende gli apparati e i mezzi di trasmissione che collegano l'utente con il nodo di accesso del gestore (es. centrale telefonica urbana).

\subsection{Rete cellulare}
L'area geografica è suddivisa in celle, e al centro di ogni cella c'è un'antenna. Le antenne sono collegate attraverso la rete dell'operatore. A differenza di una comune rete wireless, la \textbf{rete cellulare} supporta la mobilità: quando il telefonino si sposta da una cella all'altra cambia ponte radio in modo trasparente.

\subsection{Plain Old Telephone Service (POTS)}
Il \textbf{modem} è un dispositivo modulatore e demodulatore, che trasforma i bit in segnali acustici da trasmettere sulla rete telefonica analogica pubblica. Si distinguono il PC dell'utente (Data Terminal Equipment [DTE]) e il modem (Data Circuit-terminating Equipment [DCE]).

L'ultimo standard, il V.90, raggiunge (teoricamente) i 56 kb/s in ricezione e 33,6 kb/s in trasmissione. La velocità è limitata fisicamente dalla capacità di Shannon del doppino:
\[
 C_{\text{Shannon}} = B_{\text{anda}} \log{\left( \text{rapporto segnale rumore (SNR)} \right)}
 \]
dove il segnale si attenua con la lunghezza del doppino, e la banda è limitata dalla massima frequenza di suoni possibile (4 kHz).

\subsection{Integrated Services Digital Network (ISDN)}
\label{sez:isdn}
La \textbf{rete ISDN} è:
\begin{itemize}
 \item \textbf{digitale:} sulla rete viaggiano i segnali digitali fino al terminale dell'utente $\Rightarrow$ il telefono deve integrare un convertitore analogico/digitale;
 \item \textbf{integrata:} supporta la trasmissione di dati e voce su un'unica risorsa di rete.
\end{itemize}

\paragraph{Caratteristiche}
\begin{itemize}
 \item orientata alla connessione (tariffazione a tempo);
 \item pubblica e/o privata;
 \item numerica end-to-end: l'informazione viaggia tutta a livello numerico dalla sorgente alla destinazione;
 \item plesiocrona: usa trame TDM;
 \item offre servizi a circuito (telefonia, fax) e a pacchetto (trasmissione dati).
\end{itemize}

È organizzata in due tipi di flussi:
\begin{itemize}
 \item canale B (Bearer): 64 kb/s (voce, dati, fax);
\item canale D (Data): 16 kb/s (segnalazione, dati, telecontrollo);
\end{itemize}
\noindent
combinati tra loro:
\begin{itemize}
 \item Basic Rate Interface (BRI) (2B + D, 128 kb/s): destinata all'utenza domestica, il segnale numerico è distribuito tramite l'S-bus che ha una topologia a bus;
 \item Primary Rate Interface (PRI) (EU: 30B + D, USA: 23B + D): destinata alle imprese.
\end{itemize}

Gli apparati ISDN sono più costosi rispetto a quelli per l'ADSL $\Rightarrow$ è una tecnologia abbandonata.

\subsection{Digital Subscriber Line (DSL)}
\begin{figure}
	\centering
	\includegraphics[width=0.75\linewidth]{pic/04/POTS_vs_DSL.png}
\end{figure}

\noindent
La rete DSL è una rete digitale che permette di trasmettere dei dati ad alta velocità sulla rete di accesso tradizionale. L'ADSL è una DSL asimmetrica: la velocità in upstream (max 640 Kbps) è molto inferiore a quella in downstream (max 9 Mbps).

Il \textbf{filtro splitter} ha il compito di separare il segnale vocale dai dati in base alla frequenza.
\FloatBarrier

\subsection{Hybrid Fiber Coax (HFC)}
Le \textbf{reti HFC} sono le reti per la TV via cavo (CATV), diffuse negli Stati Uniti. Sono pensate per un funzionamento unidirezionale: l'operatore invia i dati ai nodi remoti via fibra ottica, e ogni nodo tramite un cavo coassiale manda il segnale in parallelo a tutti gli utenti (topologia ad albero); l'utente usa un cable modem che riceve il segnale analogico e lo converte in digitale. I dati e i segnali TV occupano porzioni diverse di banda $ \Rightarrow$ occorre un filtro presso l'utente. Siccome il canale è unidirezionale, l'upstream di dati viene effettuato su linea telefonica standard.

\paragraph{Confronto tra ADSL e HFC}
\begin{itemize}
 \item l'ADSL è punto-punto, l'HFC è un unico mezzo fisico condiviso tra tanti;
 \item l'ADSL si appoggia alla tecnologia telefonica standard presso l'utente, l'HFC richiede la posa di cavi ad hoc;
 \item nell'ADSL la qualità del segnale diminuisce con la distanza, l'HFC non risente della distanza.
\end{itemize}

\subsection{Accesso radio mobile}
\paragraph{Wireless}
GPRS, UMTS, IEEE 802.11 (wi-fi), IEEE 802.16 (Wi-Max)

\paragraph{Reti satellitari}
\begin{itemize}
 \item Geostationary Earch Orbit (GEO) (altitudine 35000 km, tempo di propagazione 270 ms, 3 satelliti necessari per la copertura globale): TV satellitare, non per upstream;
 \item Medium Earth Orbit (MEO) (altitudine 15000 km, tempo di propagazione 50 ms, \textgreater 10 satelliti): GPS;
 \item Low Earth Orbit (LEO) (altitudine \textless 1000 km, tempo di propagazione 5 ms, \textgreater 50 satelliti): telefonia satellitare con antenne omnidirezionali e bassa latenza;
 \item piattaforme stratosferiche (in fase di studio): un drone vola all'altezza della stratosfera (sopra le nuvole), e funge da satellite a bassissima quota.
\end{itemize}