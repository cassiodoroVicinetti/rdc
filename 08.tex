\chapter{Interconnessione LAN}
\paragraph{Apparati di interconnessione}
\begin{itemize}
 \item repeater o hub\footnote{Questi due termini sono qui usati come sinonimi per semplicità, ma in realtà sono due oggetti differenti.}: è in grado di interconnettere solo reti di livello 1;
 \item bridge o switch\footnotemark[\value{footnote}]: interconnette reti di livello 2 $\Rightarrow$ più complesso perché deve gestire il MAC e ha algoritmi di instradamento;
\item router: interconnette reti di livello 3, e implementa IP;
\item gateway: interconnette due reti completamente diverse e lavora a livello applicazione (livello 7).
\end{itemize}

\section{Repeater o hub}
Il \textbf{repeater} si limita solo a trasmettere i bit. L'interconnessione di due domini di collisione crea un unico dominio di collisione $\Rightarrow$ è un limite alla dimensione della rete.

\paragraph{Regeneration, reshaping, retiming (3R)}
L'unica intelligenza è a livello elettrico: si sincronizza con le onde quadre e le rigenera in modo da pulirle. La sincronizzazione richiede però un certo tempo $\Rightarrow$ il preambolo che si trova all'inizio della PDU serve per la sincronizzazione.

\paragraph{Topologia a stella}
Un hub si collega a più nodi, e il segnale elettrico proveniente da un nodo viene replicato su tutte le altre interfacce. Siccome il cavo UTP consente solo collegamenti punto-punto, ogni nodo deve essere collegato direttamente con l'hub:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.2\linewidth]{pic/08/Network_hub.png}
\end{figure}

Non sono permessi anelli perché il segnale comincia a girare e inizia una collisione infinita:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.15\linewidth]{pic/08/Loop_of_hubs.png}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.66\linewidth]{pic/08/Esempio_interconnessione_hub.png}
\end{figure}

\section{Bridge o switch}
\begin{figure}
	\centering
	\includegraphics[width=0.25\linewidth]{pic/08/Network_switch_(standard_notation).png}
\end{figure}

\noindent
I \textbf{bridge} implementano il collision detect (CS), e si basano su store and forward. Un bridge può interconnettere anche livelli fisici e MAC diversi, perché è in grado di fare la traduzione delle intestazioni, purché i protocolli di livello superiore (LLC) siano uguali. I bridge non intervengono sul contenuto dei pacchetti.

L'intelligenza di instradamento è molto semplice: quando arriva il segnale:
\begin{itemize}
 \item lo memorizza (store);
\item verifica se è valido;
\item verifica la destinazione: se il pacchetto è destinato a un nodo in un altro dominio di collisione:
\begin{itemize}
 \item sente se la rete è occupata (CS);
\item lo trasmette all'altro dominio di collisione (forward).
\end{itemize}
\end{itemize}
\FloatBarrier

L'interconnessione dev'essere trasparente all'utente. Un insieme di segmenti di LAN interconnessi mediante bridge è detto anche \textbf{LAN estesa}:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.75\linewidth]{pic/08/Esempio_di_LAN_estesa.png}
\end{figure}

Collegare due PC, cioè due schede di rete, a due terminali separati di uno switch rende superfluo il collision detect:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.15\linewidth]{pic/08/Network_switch.png}
\end{figure}

In questo caso conviene realizzare un collegamento \textbf{full-duplex}, cioè con CS disabilitato.

Il bridge interrompe la condivisione di risorse trasmissive tipica della LAN, creando due domini di collisione separati, perché la comunicazione tra due schede di un dominio non interferisce con la comunicazione tra due schede di un altro dominio. Nel caso il pacchetto abbia destinazione broadcast (FFFF\dots), il dominio invece è unico. Lo switch quindi permette di estendere l'estensione geografica della rete senza problemi. Anche lo switch rigenera il segnale. Lo switch introduce anche sicurezza perché un nodo non può sniffare comunicazioni tra altri nodi.\footnote{Esistono in realtà dei metodi.}

\subsection{Svantaggi}
\begin{itemize}
 \item ritardi di store and forward rispetto all'hub:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.75\linewidth]{pic/08/Prestazioni_hub_vs_switch.png}
\end{figure}

\item possibilità di perdita di pacchetti per overflow/congestione delle code di trasmissione:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.66\linewidth]{pic/08/Problema_congestione_code_switch.png}
\end{figure}

\item problemi di equità nella condivisione della banda (gli hub invece garantiscono che la banda sia spartita equamente tra tutti):
\begin{figure}[H]
	\centering
	\includegraphics[width=0.75\linewidth]{pic/08/Problemi_switch.png}
\end{figure}
\end{itemize}

\subsection{Transparent bridge}
Ogni porta dello switch deve avere un proprio indirizzo MAC. L'instradamento è svolto in maniera trasparente: lo switch cerca di imparare la posizione dei nodi ad esso collegati riempendo una forwarding table.

Se l'utente sposta il PC in un'altra porta, lo switch deve supportare lo spostamento e correggere la forwarding table. C'è un timer: oltre un certo tempo un MAC memorizzato si cancella.

\begin{figure}
	\centering
	\includegraphics[width=0.66\linewidth]{pic/08/Address_learning_e_frame_forwarding.png}
	\caption{Address learning e frame forwarding.}
\end{figure}

\subsubsection{Address learning}
Nell'\textbf{address learning} l'apprendimento è basato sugli indirizzi MAC sorgenti dei pacchetti, tramite un algoritmo di \textbf{backward learning}.

\subsubsection{Frame forwarding}
Nel \textbf{frame forwarding} l'apprendimento è basato sull'indirizzo MAC di destinazione: quando arriva un pacchetto la cui destinazione non è ancora presente nella forwarding table, lo switch manda il pacchetto in broadcast (a tutti), e attende la risposta che molto probabilmente la destinazione invierà.
\FloatBarrier

\subsubsection{Spanning tree}
L'algoritmo di backward learning funziona solo se in topologia non ci sono degli anelli, altrimenti i nodi continuano a ricevere pacchetti identici all'infinito, oppure l'invio di pacchetti si blocca:
\begin{figure}[H]
	\centering
	\includegraphics[width=\linewidth]{pic/08/Criticita_algoritmo_di_backward_learning.png}
\end{figure}

L'\textbf{algoritmo spanning tree} serve per eliminare gli anelli logici dalla topologia fisica: quando si verifica un guasto lo switch ricalcola automaticamente uno spanning tree per evitare le criticità.

\subsubsection{Limiti}
\begin{itemize}
 \item non si possono aggiungere degli switch per creare dei collegamenti alternativi che si spartiscano il traffico perché si creerebbero degli anelli in topologia;
\item se la rete è molto grande, la forwarding table diventa molto grande;
\item il traffico broadcast dev'essere confinato, cioè non dev'essere mandato indistintamente a tutti i nodi.
\end{itemize}