\chapter{Livello 2 OSI}
\label{cap:livello_2_osi}
Il \textbf{livello di collegamento} (data link) permette il trasferimento di unità dati del livello rete e cerca di fronteggiare i malfunzionamenti dello strato fisico.

\paragraph{Funzioni}
\begin{itemize}
 \item delimitare le trame, cioè capire dove si trovano l'inizio e la fine di una PDU, in diversi modi:
\begin{itemize}
 \item vi sono bit che delimitano esplicitamente la PDU o ne indicano la lunghezza;
\item i pacchetti sono a lunghezza fissa;
\item la fine di un pacchetto e l'inizio di un altro corrisponde a un silenzio nella trasmissione;
\end{itemize}
\item multiplare i flussi provenienti dagli strati superiori in un mezzo fisico;
\item risolvere i problemi di indirizzamento;
\item rilevare gli errori;
\item utilizzare i protocolli a finestra\footnote{Si veda il capitolo~\ref{cap:protocolli_finestra}.} per:
\begin{itemize}
\item fare il controllo di flusso, cioè evitare di inondare la destinazione se non è pronta;
\item controllare i numeri di sequenza;
\item correggere gli errori;
\end{itemize}
\item regolare l'accesso multiplo nei canali condivisi;
\item fare il controllo di flusso sull'interfaccia verso i livelli superiori.
\end{itemize}

\section{HDLC}
L'\textbf{High-level Data Link Control} (HDLC) è lo standard ISO del protocollo Syncronous Data Link Control (SDLC), da cui derivano:
\begin{itemize}
 \item LAP-B (sezione~\ref{sez:lapb});
 \item LLC per reti locali (sezione~\ref{sez:llc});
 \item PPP per collegamenti punto-punto su linea commutata (sezione~\ref{sez:ppp});
 \item LAP-F per reti Frame Relay (sezione~\ref{sez:lapf}).
\end{itemize}

Le PDU hanno il seguente formato:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.75\linewidth]{pic/05/Formato_PDU_HDLC.png}
\end{figure}

\subsection{Campi di delimitazione}
01111110 è la sequenza che delimina l'inizio e la fine di una PDU, ma è la combinazione vietata nei dati. Nasce il problema se l'utente vuole trasferire proprio la sequenza vietata.

Si può implementare a livello hardware una regola di tipo \textbf{bit-stuffing}: se viene ricevuta una sequenza di 5 bit a 1:
\begin{itemize}
 \item se il sesto bit è un 1: la PDU è terminata;
 \item se il sesto bit è uno 0: il sesto bit viene ignorato e la lettura continua al settimo bit.
\end{itemize}

In questo modo se l'utente vuole inviare la sequenza 01111110, la PDU conterrà la sequenza 011111\textsl{0}10.

\subsection{Campo CRC}
Il codice CRC segue i dati perché così l'apparato di ricezione può iniziare a calcolare il CRC dei dati appena sono stati ricevuti mentre aspetta di ricevere anche il CRC di controllo.

\subsection{Campo di controllo}
\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{pic/05/Formato_campo_di_controllo_PDU_HDLC.png}
\end{figure}

\noindent
Il campo di controllo differenzia le PDU:
\begin{itemize}
 \item N(S): numero di sequenza;
\item N(R): numero di riscontro;
\item P/F: bit di poll (se inviato dalla stazione primaria) o bit di final (se inviato dalla stazione secondaria). L'invio di un poll specifica che il mittente attende obbligatoriamente una risposta; il final è la risposta a un poll. Il mittente non può emettere un altro P = 1 prima di aver ricevuto un F = 1, perché altrimenti non saprebbe a quale richiesta risponderebbe il final.
\end{itemize}
\FloatBarrier

\section{Link Access Procedure Balanced-B (LAP-B)}
\label{sez:lapb}
\subsection{Campo di controllo}
\subsubsection{Trame di informazione (I)}
Le \textbf{trame di informazione} permettono di trasferire i dati.

\begin{itemize}
 \item N(S) = numero di sequenza della PDU trasmessa;
\item N(R) = numero di sequenza della PDU attesa (ACK).
\end{itemize}

È quindi possibile il piggybacking:\footnote{Si veda la sezione~\ref{sez:piggybacking}.} il trasmettitore può mandare insieme ai dati anche l'ACK relativo al flusso di dati in direzione inversa.

\subsubsection{Trame di supervisione (S)}
Le \textbf{trame di supervisione} permettono di trasferire ACK senza mandare dei dati. I due bit SS possono comunicare tre informazioni di stato:
\begin{itemize}
 \item RR (ricevitore pronto): fornisce riscontro positivo;
 \item RNR (ricevitore non pronto): fornisce riscontro positivo, ma dichiara che il ricevitore non è disponibile (controllo di flusso);
 \item REJ (reject): richiede di ritrasmettere tutte le PDU a partire da N(R).
\end{itemize}

\subsubsection{Trame non numerate (U)}
Le \textbf{trame non numerate} permettono il controllo della trasmissione, con comandi e risposte sui 5 bit M:
\begin{itemize}
 \item Set Asynchronous Balanced Mode(E) (SABM(E)): (re)inizializza il collegamento, specificando anche se la numerazione delle PDU è su 3 o 7 bit;
\item Disconnect (DISC): annuncia la chiusura del collegamento;
\item Unnumbered Acknowledgment (UA): ACK alla PDU di tipo SABM o DISC.
\end{itemize}

\subsection{Campo di indirizzo}
L'indirizzo non sempre è quello del destinatario: nel caso di una risposta viene specificato l'indirizzo del mittente.

\subsection{Tecniche di recupero degli errori}
Nel LAP-B sono implementabili varie tecniche di recupero degli errori:
\begin{figure}[H]
	\centering
	\includegraphics[width=\linewidth]{pic/05/Recupero_errori_LAP-B.png}
\end{figure}

\section{LLC}
\label{sez:llc}
Nelle reti locali il livello 2 è diviso in due sottolivelli: \textbf{Logical Link Control} (LLC) e \textbf{Medium Access Control} (MAC).

IEEE 802.2 è lo standard ISO dell'LLC.

Il formato delle PDU LLC è:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.75\linewidth]{pic/05/Formato_PDU_LLC.png}
\end{figure}

Mancano i campi di delimitazione e di CRC perché sono demandati al MAC. Il campo di controllo è di 1 byte nelle PDU non numerate, e di 2 nelle PDU numerate. Il campo dati ha una dimensione multipla del byte. Il campo di indirizzo contiene gli indirizzi delle SAP\footnote{Si veda la sezione~\ref{sez:SAP}.} sorgente e di destinazione.

Deve essere specificato anche quale protocollo usa il livello superiore.

\subsection{SNAP}
Il \textbf{SubNetwork Access Protocol} (SNAP) è una particolare implementazione dell'LLC. Il formato delle PDU è:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.75\linewidth]{pic/05/Formato_PDU_SNAP.png}
\end{figure}

Nel campo dati, 5 byte vengono usati per definire il protocollo di livello superiore usato dalla PDU contenuta. In particolare, il campo OUI è assegnato in modo univoco a un ente o società, che può definire i propri tipi di protocollo con i due byte a fianco.

\section{PPP}
\label{sez:ppp}
Il \textbf{Point to Point Protocol} (PPP) è utilizzato per collegamenti punto-punto cablati:
\begin{itemize}
 \item collegamenti su linea telefonica tra il modem dell'utente e il provider Internet;
\item connessioni SONET/SDH;
\item circuiti ISDN.
\end{itemize}

\paragraph{Obiettivi}
\begin{itemize}
 \item sono presenti i campi di delimitazione delle PDU (detti flag);
\item riconosce gli errori ma non li corregge;
\item multipla più protocolli di livello rete;
\item controlla l'attività del collegamento;
\item permette la negoziazione dell'indirizzo di livello rete (tipicamente IP): i nodi ai due estremi del collegamento apprendono o configurano i propri indirizzi di rete.
\end{itemize}

Non si occupa né di controllo di flusso né di mantenimento della sequenza.

Il formato delle PDU è:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.75\linewidth]{pic/05/Formato_PDU_PPP.png}
\end{figure}

I campi di indirizzo e di controllo non hanno significato perché in una comunicazione punto-punto non serve l'indirizzo. Il campo di protocollo ha funzione analoga a quello dell'LLC SNAP.

Lo stuffing è a livello di byte: esiste un byte di escape (01111101) che interviene sia quando nei dati compare un byte uguale al byte di delimitazione (01111110), sia quando compare un byte uguale al byte di escape stesso:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.33\linewidth]{pic/05/PPP_byte_stuffing}
\end{figure}

Il protocollo PPP-Link Control Protocol (PPP-LCP) ha il compito di aprire e chiudere una connessione PPP, negoziando alcune opzioni (lunghezza massima dei frame, protocollo di autenticazione, ecc.).

\section{LAP-F}
\label{sez:lapf}
Le \textbf{reti Frame Relay} sono reti a pacchetto con servizio di circuito virtuale (tipicamente permanenti):
\begin{itemize}
 \item velocità: tra 64 kb/s e 2 Mb/s;
\item dimensione pacchetti: variabile, max 4 KiB.
\end{itemize}

Le reti Frame Relay utilizzano il protocollo Link Access Procedure to Frame mode bearer services (LAP-F).

Il formato delle PDU è uguale a quello dell'HDLC, ma i campi si dividono in due categorie:
\begin{itemize}
 \item DL-CORE (flag + indirizzo + CRC): utilizzato da tutti i nodi della rete;
\item DL-CONTROL (controllo): utilizzato solo dal mittente e dal destinatario.
\end{itemize}

Il campo di indirizzo è composto da alcuni sottocampi, tra cui:
\begin{itemize}
\item DLCI: identifica il circuito virtuale;
\item FECN e BECN: avvisano esplicitamente la sorgente e la destinazione che un nodo della rete è congestionato (il TCP/IP si accorge implicitamente di congestioni tenendo sotto controllo le perdite di pacchetti);
\item DE: specifica la priorità del pacchetto;
\item C/R: distingue se il pacchetto è un comando o una risposta.
\end{itemize}

\section{ATM}
Le \textbf{reti Asynchronous Transfer Mode} (ATM) sono reti B-ISDN\footnote{Si veda la sezione~\ref{sez:isdn}.} a pacchetto con servizio di circuito virtuale. La dimensione dei pacchetti è fissa: 53 byte, di cui 48 byte per i dati e 5 byte per l'intestazione.

\paragraph{Svantaggi}
\begin{itemize}
 \item la dimensione dei pacchetti è fissa → per trasportare un singolo byte di informazione servono altri 52 byte;
\item la dimensione dei pacchetti è piccola → molta banda è usata per le intestazioni (quasi il 10\%).
\end{itemize}

\paragraph{Vantaggi}
\begin{itemize}
 \item velocità elevate (min. 622 Mb/s);
\item la regolarità del formato ATM velocizza il processo di elaborazione dei pacchetti e semplifica l'hardware;
\item bassa latenza e basso ritardo di pacchettizzazione $\Rightarrow$ adatto per il trasporto di voce (telefonia) e video.
\end{itemize}

Le reti ATM hanno un modello molto complesso, derivato da una mentalità ``da operatore telefonico'' per avere il controllo su tutta la rete e garantire un'alta affidabilità ai guasti.

\subsection{Strato di adattamento ad ATM (AAL)}
Quando ATM era stato progettato, si pensava a PC dotati direttamente di schede ATM. Oggi in realtà i PC hanno implementato solo il protocollo IP, e l'ATM è usato solo in alcune parti della rete che richiedono una certa affidabilità, ma servono dei router che convertano in maniera trasparente i pacchetti ATM in pacchetti IP e viceversa.

Le reti ATM hanno un approccio core and edge: al di sopra dello strato ATM (core) vi è uno strato di adattamento (edge), detto ATM Adaptation Layer (AAL), che è presente solo nel terminale di sorgente e in quello di destinazione, ed è appunto usato per suddividere i pacchetti IP in pacchetti ATM.

Il livello AAL è in grado di gestire:
\begin{itemize}
\item gli errori di trasmissione;
\item la pacchettizzazione;
\item la perdita di pacchetti;
\item il controllo di flusso.
\end{itemize}

\subsubsection{Classi di servizio}
Sono definite 4 classi di servizio AAL in base a 3 parametri legati alla qualità del servizio:
\begin{table}[H]
 \centering
 \begin{minipage}{\textwidth}
  \centering
\centerline{
 \begin{tabular}{|c|c|c|c|c|}
  \cline{2-5}
   \multicolumn{1}{c|}{} & \textbf{Classe A} & \textbf{Classe B} & \textbf{Classe C} & \textbf{Classe D}\\
   \hline
   \begin{tabular}[c]{@{}c@{}}\textbf{Modalità di temporizzazione}\\fra sorgente e destinazione\end{tabular} & \multicolumn{2}{|c|}{necessaria\footnote{Nelle applicazioni real-time (ad es. voce) ci sono dei vincoli temporali, cioè i pacchetti non devono arrivare con eccessivo ritardo.}} & \multicolumn{2}{|c|}{non necessaria\footnote{Non vi sono particolari vincoli temporali, basta che i pacchetti arrivino.}}\\
   \hline
  \begin{tabular}[c]{@{}c@{}}\textbf{Velocità di trasmissione}\\\textbf{della sorgente}\end{tabular} & costante (CBR) & \multicolumn{3}{|c|}{variabile (VBR)}\\
  \hline
  \textbf{Modalità di connessione} & \multicolumn{3}{|c|}{orientato alla connessione\footnote{Sono sfruttate tutte le caratteristiche di un circuito virtuale.}} & non connesso\footnote{Il circuito virtuale non viene sfruttato a livello superiore.}\\
  \hline
  \textbf{Tipi di PDU AAL} & AAL tipo 1 & AAL tipo 2 & \multicolumn{2}{|c|}{AAL tipo 3/4 - 5}\\
  \hline
  \textbf{Possibili applicazioni} & \begin{tabular}[c]{@{}c@{}}voce 64 kbit/s\\video CBR\end{tabular} & video/audio VBR & \multicolumn{2}{|c|}{dati}\\
  \hline
 \end{tabular}
}
 \end{minipage}
\end{table}

Il livello AAL è ancora suddiviso in due sottolivelli: CS e SAR. Di seguito è trattato il processo che porta a una PDU ATM di tipo 5.

\subsubsection{PDU CS}
\begin{figure}
	\centering
	\includegraphics[width=0.5\linewidth]{pic/05/Formato_PDU_CS.png}
\end{figure}

\noindent
Il pacchetto di livello 3 (tipicamente IP) viene tagliato in parti di dimensione massima 65535 byte, che diventano il payload delle PDU Convergence Sublayer (CS). Il padding sono dei bit di riempimento per adattare il payload CS ad un multiplo di 48, e quindi può andare da 0 a 47 byte. Siccome la lunghezza del payload è variabile, un campo di 2 byte ne definisce la lunghezza. Alla fine il codice CRC permette di verificare la correttezza dell'intera PDU CS al momento della ricostruzione, e quindi di rilevare la perdita di un pacchetto ATM.
\FloatBarrier

\subsubsection{PDU SAR}
\begin{figure}
	\centering
	\includegraphics[width=0.5\linewidth]{pic/05/SAR_PDU_format.png}
\end{figure}

\noindent
I blocchi da 48 byte vengono inframezzati dalle PCI ATM. Nell'ultima PCI ATM, il terzo bit del campo PT viene impostato a 1 per segnalare la fine del segmento.
\FloatBarrier

\subsection{PDU ATM}
\begin{figure}
	\centering
	\includegraphics[width=0.66\linewidth]{pic/05/ATM_PDU_format.png}
\end{figure}

\noindent
Il formato della PCI distingue due tipi di PDU ATM:
\begin{itemize}
 \item User-to-Network Interface (UNI): per la comunicazione tra l'utente e un nodo della rete (VPI da 8 bit);
 \item Network-to-Network Interface (NNI): per la comunicazione tra due nodi interni alla rete (VPI da 12 bit).
\end{itemize}

\begin{description}
 \item[Generic Flow Control (GFC) (4 bit)] \hfill \\
 Tramite il GFC la rete può informare l'utente quanti dati può ricevere (controllo di flusso).
 \item[Virtual Path Identifier (VPI) (8-12 bit)] \hfill \\
 Il VPI è l'identificativo del virtual path, che è un aggregato di circuiti virtuali.
 \item[Virtual Circuit Identifier (VCI) (16 bit)] \hfill \\
 Il VCI è l'identificativo del circuito virtuale all'interno di un virtual path.
 \item[Payload Type (PT) (3 bit)] \hfill \\
Il PT identifica il tipo di dati trasportati:
\begin{itemize}
\item 4 codici sono riservati alle funzioni d'utente, e possono avvisare esplicitamente della presenza di congestioni per rallentare la sorgente;
\item 4 codici sono riservati alle funzioni di rete (gestione delle risorse e manutenzione della rete).
\end{itemize}
\item[Cell Loss Priority (CLP) (1 bit)] \hfill \\
Il CLP assegna al pacchetto una priorità per differenziare il traffico: se è presente una congestione, il nodo inizia a buttare via i pacchetti a priorità bassa (CLP = 1) preservando quelli a priorità alta (CLP = 0). In alcuni casi tipo il traffico vocale, se ogni tanto viene perso qualche pacchetto non importa.
\item[Header Error Code (HEC) (8 bit)] \hfill \\
L'HEC permette di capire se l'intestazione è stata ricevuta correttamente, e permette di correggere un singolo errore oppure di rilevare due errori.
\end{description}
\FloatBarrier

\begin{table}[H]
 \centering
 \caption{Confronto tra protocolli di strato 2.}
\centerline{
 \begin{tabular}{|c|c|c|c|c|}
  \hline
  \textbf{Protocollo} & \begin{tabular}[c]{@{}c@{}}\textbf{Delimitazione}\\\textbf{pacchetti}\end{tabular} & \begin{tabular}[c]{@{}c@{}}\textbf{Multiplazione}\\\textbf{protocolli di strato 3}\end{tabular} & \begin{tabular}[c]{@{}c@{}}\textbf{Rilevazione}\\\textbf{errori}\end{tabular} & \begin{tabular}[c]{@{}c@{}}\textbf{Correzione errori}\\\textbf{(protocollo a finestra)}\end{tabular} \\
  \hline
  \textbf{LAP-B} & delimitatori & \begin{tabular}[c]{@{}c@{}}realizzato in\\strato superiore\end{tabular} & sì & sì \\
  \hline
  \textbf{LLC} & \begin{tabular}[c]{@{}c@{}}demandato\\a MAC\end{tabular} & sì & opzionale & opzionale \\
  \hline
  \begin{tabular}[c]{@{}c@{}}\textbf{MAC}\\\textbf{(Ethernet)}\end{tabular} & silenzi & sì & sì & no \\
  \hline
  \textbf{PPP} & delimitatori & sì & sì & no \\
  \hline
  \textbf{LAP-F} & delimitatori & \begin{tabular}[c]{@{}c@{}}mediante\\circuiti virtuali\end{tabular} & sì (DL-CORE) & \begin{tabular}[c]{@{}c@{}}opzionale\\(DL-CONTROL)\end{tabular} \\
  \hline
  \textbf{ATM} & \begin{tabular}[c]{@{}c@{}}demandato al\\livello fisico\end{tabular} & \begin{tabular}[c]{@{}c@{}}mediante\\circuiti virtuali\end{tabular} & sì (AAL) & no \\
  \hline
 \end{tabular}
}
\end{table}