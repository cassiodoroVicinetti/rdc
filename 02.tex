\chapter{OSI}
Il \textbf{modello Open System Interconnection} (OSI) è il modello di riferimento per le reti di telecomunicazione a pacchetto. È recepito nei seguenti standard: ISO IS 7498 e CCITT X.200. È organizzato gerarchicamente in livelli:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.66\linewidth]{pic/02/Abbinamento_ISO-OSI_e_TCP-IP.jpg}
	\caption[]{Implementazione del modello OSI nell'Internet Protocol Suite.\footnotemark}
\end{figure}
\footnotetext{Questa immagine è tratta da Wikimedia Commons (\href{https://commons.wikimedia.org/wiki/File:Abbinamento_ISO-OSI_e_TCP-IP.jpg}{Abbinamento ISO-OSI e TCP-IP.jpg}), è stata realizzata da Daniele Giacomini ed è concessa sotto la \href{http://creativecommons.org/licenses/by-sa/2.5/deed.it}{licenza Creative Commons Attribuzione - Condividi allo stesso modo 2.5 Generico}.}

Una rete si può definire rete Internet se a livello rete vi è implementato il protocollo IP. In una rete Internet i 3 livelli superiori sono lasciati agli applicativi sul PC dell'utente.

Le reti B-ISDN (es. ATM) hanno delle architetture molto più complesse ma consentono una maggiore affidabilità.

\section{Nomenclatura}
\begin{figure}
	\centering
	\includegraphics[width=0.66\linewidth]{pic/02/Nomenclatura_modello_OSI.png}
	\caption{Nomenclatura.}
\end{figure}

\noindent
In un \textbf{sistema} gli \textbf{strati} sono organizzati in modo gerarchico: ogni strato fornisce servizi allo strato superiore, usando i servizi dello strato inferiore (detto \textbf{black-box}) e le proprie determinate funzioni.

Le \textbf{entità} sono gli elementi attivi di un \textbf{sottosistema} e interagiscono all'interno di uno strato. Ad esempio, le entità che si trovano al livello più alto sono gli applicativi in esecuzione sul PC dell'utente.
\FloatBarrier

\section{Strati}
\begin{figure}
	\centering
	\includegraphics[width=0.6\linewidth]{pic/02/OSI_Model_v1}
	\caption[]{Modello OSI.\footnotemark}
\end{figure}
\footnotetext{Questa immagine è tratta da Wikimedia Commons (\href{https://commons.wikimedia.org/wiki/File:OSI_Model_v1.svg}{OSI Model v1.svg}), è stata realizzata da \href{https://en.wikipedia.org/wiki/User:Dino.korah}{Dino Korah} e dall'utente \href{https://commons.wikimedia.org/wiki/User:Offnfopt}{Offnfopt} ed è concessa sotto la \href{https://creativecommons.org/publicdomain/zero/1.0/deed.it}{licenza Creative Commons CC0 1.0 Universal}.}

\noindent
I sistemi si distinguono in:
\begin{itemize}
\item \textbf{sistemi terminali:} implementano tutti i livelli;
\item \textbf{sistemi di rilegamento} (relay): implementano sono alcuni dei livelli più bassi (es. switch Ethernet).
\end{itemize}

\marginpar{\footnotesize{[non chiaro]}}
Nelle reti pubbliche gli strati di utilizzazione, che corrispondono all'utente, accedono alla sezione di accesso della rete tramite protocolli di accesso, e i nodi della rete interna comunicano tra di loro attraverso protocolli di rete.

\subsection[Livello 1: fisico]{Livello 1: fisico\footnote{Per approfondire, si veda il capitolo~\ref{cap:livello_1_osi}.}}
Il \textbf{livello fisico} (physical) è lo strato deputato al trasporto fisico dei bit di informazione tra un sistema e l'altro. Un livello fisico è definito in base a codifiche di linea, connettori, livelli di tensione, ecc.

\subsection[Livello 2: collegamento]{Livello 2: collegamento\footnote{Per approfondire, si veda il capitolo~\ref{cap:livello_2_osi}.}}
Il \textbf{livello di collegamento} (data link) permette il trasferimento di unità dati del livello rete e cerca di fronteggiare i malfunzionamenti dello strato fisico.

\paragraph{Funzioni}
\begin{itemize}
 \item rivelare e recuperare gli errori di trasmissione;
 \item controllare il flusso, cioè evitare di inondare la destinazione se non è pronta a ricevere dati;
 \item delimitare le unità dati, cioè individuare l'inizio e la fine di una PDU, di una PCI, e così via.
\end{itemize}

\subsection[Livello 3: rete]{Livello 3: rete\footnote{Per approfondire, si veda il capitolo~\ref{cap:protocollo_ipv4}.}}
Il \textbf{livello di rete} (network) controlla:
\begin{itemize}
 \item l'instradamento, cioè trova il percorso all'interno della rete;
 \item il flusso, cioè evita di inondare la destinazione;
 \item la congestione, cioè evita di intasare la rete;
 \item la tariffazione.
\end{itemize}

Siccome l'Internet Protocol Suite non segue rigorosamente lo standard, i controlli di flusso, congestione e tariffazione \ul{non} vengono svolti nel livello di rete (IP) ma nel livello di trasporto.

\subsection[Livello 4: trasporto]{Livello 4: trasporto\footnote{Per approfondire, si veda il capitolo~\ref{cap:livello_trasporto}.}}
Il \textbf{livello di trasporto} (transport):
\begin{itemize}
 \item effettua controlli di errore e di sequenza: cerca di garantire una certa qualità di servizio riparando gli errori che si verificano nello strato di rete;
 \item controlla il flusso, cioè evita di inondare la destinazione;
 \item gestisce la multiplazione, la suddivisione, la segmentazione e la concatenazione dei pacchetti.
\end{itemize}

Nell'Internet Protocol Suite il livello di trasporto controlla anche la congestione, ed è il primo livello ad essere implementato solo nel PC dell'utente, mentre il livello di rete è ancora presente in tutti gli apparati di commutazione.

\subsection{Livello 5: sessione}
Il \textbf{livello di sessione} (session) gestisce lo scambio di dati a livello temporale. Ad esempio, in una applicazione che riproduce una sorgente video il livello di sessione è quello che ricostruisce l'ordine corretto dei fotogrammi e gestisce la funzione di pausa. Su Internet questo livello è interamente all'interno dell'applicazione stessa.

\subsection{Livello 6: presentazione}
Il \textbf{livello di presentazione} (presentation) controlla come i dati vengono rappresentati, risolve eventuali problemi di compatibilità e può fornire anche servizi di cifratura dei dati.

\subsection[Livello 7: applicazione]{Livello 7: applicazione\footnote{Per approfondire, si veda il capitolo~\ref{cap:livello_applicazione}.}}
Il \textbf{livello di applicazione} (application) è lo strato che interagisce con l'utente.
\FloatBarrier

\section{Comunicazione}
\subsection{Protocollo}
Il \textbf{protocollo} è un insieme di convenzioni prestabilite. Attraverso un protocollo un'entità può comunicare con un'altra entità che si trova sullo stesso livello gerarchico, anche in sistemi diversi.

Definire un protocollo significa definire:
\begin{itemize}
 \item algoritmi: la semantica del protocollo (i significati delle parole);
 \item formati: la sintassi del protocollo (la costruzione delle parole);
 \item temporizzazione: le sequenze temporali.
\end{itemize}

\subsection{Service Access Point (SAP)}
\label{sez:SAP}
Attraverso un \textbf{Service Access Point} (SAP) un'entità può comunicare con un'altra entità di livello inferiore o superiore.

Ad ogni entità è associato un \textbf{titolo}. Ad ogni SAP è associato un \textbf{indirizzo}.

La connessione tra entità può essere:
\begin{itemize}
 \item \textbf{multiplata:} più entità di livello superiore condividono un'unica entità di livello inferiore;
 \item \textbf{suddivisa:} un'entità di livello superiore è collegata a più entità di livello inferiore.
\end{itemize}

\subsection{Creazione PDU}
La PDU di livello superiore diventa la SDU di livello inferiore, e ogni livello aggiunge la sua PCI:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.33\linewidth]{pic/02/TCP-IP_encapsulation.png}
\end{figure}

Ogni volta che un'entità crea una PDU usando come SDU la PDU di livello superiore cambia il protocollo da usare per leggere la PDU.

A ogni passaggio le dimensioni della PDU aumentano sempre di più. Possono esistere dei vincoli relativi alle dimensioni dei pacchetti:
\begin{itemize}
 \item \textbf{segmentazione:} il pacchetto dello strato superiore viene separato in più parti, e ogni parte viene inserita in una diversa SDU;
 \item \textbf{concatenazione:} più pacchetti dello strato superiore vengono caricati tutti su una sola SDU.
\end{itemize}

Il processo continua fino a quando il pacchetto arriva ai mezzi trasmissivi, che lo trasferiscono fisicamente a un altro sistema.

\section{Servizio}
Un servizio può essere:
\begin{itemize}
 \item \textbf{connection-oriented} (CO): prima di trasferire i dati il trasmittore si assicura che il ricevitore sia raggiungibile;
 \item \textbf{connectionless} (CL): send \& pray.
\end{itemize}

Ad esempio, per trasferire informazioni tra un'entità e un'altra entità dello stesso livello ma di un altro sistema è necessario un fornitore del servizio di livello inferiore:
\begin{itemize}
 \item trasferimento con connessione (CO): esiste un accordo preliminare tra le tre parti entità-fornitore-entità che precede l'invio delle informazioni;
 \item trasferimento senza connessione (CL): sono necessari accordi tra le singole parti che sono indipendenti tra loro: un accordo tra l'entità sorgente e quella di destinazione, un accordo tra l'entità sorgente e il fornitore del servizio, e un accordo tra il fornitore del servizio e l'entità di destinazione.
\end{itemize}

\begin{figure}
	\centering
	\includegraphics[width=0.5\linewidth]{pic/02/Primitive_entita_OSI.png}
\end{figure}

Le \textbf{primitive} sono le interazioni tra le entità che permettono di offrire un servizio:
\begin{itemize}
 \item \textbf{richiesta:} la primitiva chiamata dall'entità trasmettitore verso l'entità fornitore del servizio;
 \item \textbf{indicazione:} la primitiva chiamata dall'entità fornitore del servizio verso l'entità ricevitore;
 \item \textbf{risposta:} la primitiva chiamata dall'entità ricevitore verso l'entità fornitore del servizio;
 \item \textbf{conferma:} la primitiva chiamata dall'entità fornitore del servizio verso l'entità trasmettitore.
\end{itemize}

La chiamata di una primitiva genera le chiamate di altre primitive di livello inferiore fino ad arrivare ai mezzi trasmissivi.
%\FloatBarrier