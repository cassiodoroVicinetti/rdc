\chapter{Application Layer}
\label{cap:livello_applicazione}
\begin{figure}
	\centering
	\includegraphics[width=0.5\linewidth]{pic/19/Socket_nella_pila_OSI.png}
\end{figure}

\noindent
Il \textbf{socket} di un processo applicativo è il canale di comunicazione con il livello trasporto: il processo applicativo di un host e il processo applicativo di un altro host si scambiano messaggi attraverso i loro socket.

La comunicazione tra due host può avvenire secondo il paradigma client-server o in maniera peer-to-peer (P2P).

Le applicazioni possono avere diverse esigenze a seconda della loro funzione:
\begin{itemize}
 \item \textbf{integrità dei dati:} un'e-mail deve arrivare intera, mentre una telefonata può tollerare la perdita di qualche campione;
\item \textbf{ritardi:} una telefonata è molto sensibile ai ritardi nella conversazione;
\item \textbf{throughput:} le applicazioni multimediali non funzionano se la rete è troppo carica, mentre le \textbf{applicazioni elastiche} come il caricamento di una pagina Web sono in grado di adattarsi al throughput corrente della rete;
\item \textbf{sicurezza:} il protocollo Secure Sockets Layer (SSL) fornisce una connessione TCP criptata per motivi di sicurezza.
\end{itemize}

Le applicazioni usano vari \textbf{protocolli di livello applicazione}: SMTP per la posta elettronica, HTTP per le pagine Web, ecc.
\FloatBarrier

\section{Protocollo HTTP}
Il \textbf{protocollo Hypertext Transfer Protocol} (HTTP) serve per il recupero di pagine Web dal server. Utilizza il TCP (tipicamente sulla well-known port 80) perché non sono ammesse perdite di pacchetti.

\subsection{Persistenza}
La versione 1.0 di HTTP è di tipo \textbf{non persistente}: viene aperta una connessione TCP per ogni oggetto (immagini) che costituisce la pagina Web.

\begin{figure}
	\centering
	\includegraphics[width=0.5\linewidth]{pic/19/Prestazioni_HTTP_non_persistente.png}
\end{figure}

La ricezione di ogni oggetto richiede l'attesa di 2 RTT + il tempo di trasmissione del file:
\begin{enumerate}
 \item viene aperta la connessione TCP tramite three-way handshake;\footnote{Si veda la sezione~\ref{sez:syn_ack}.}
\item viene trasferito l'oggetto richiesto dal client.
\end{enumerate}
\FloatBarrier

La versione 1.1 di HTTP è di tipo \textbf{persistente:} tutti gli oggetti della pagina Web vengono scaricati entro un'unica connessione TCP $\Rightarrow$ risparmio di tempo.

\subsection{Messaggi HTTP}
\paragraph{Messaggi di richiesta}
Il client invia dei \textbf{messaggi di richiesta}, nella cui intestazione specifica alcuni campi ASCII tra cui il tipo di comando:
\begin{itemize}
 \item \texttt{GET}: richiesta di un oggetto, ad esempio:\\
 \centerline{\texttt{GET /index.html HTTP/1.1}}
\item \texttt{POST}: invio di input da form, contenuto nel corpo del messaggio;
\item \texttt{HEAD}: uguale a GET, ma il server invia nel messaggio di risposta solo l'intestazione e non il contenuto dell'oggetto;
\item \texttt{PUT}: invio di un file nel corpo del messaggio (solo HTTP/1.1);
\item \texttt{DELETE}: eliminazione di un file (solo HTTP/1.1).
\end{itemize}

L'intestazione contiene anche il campo \texttt{Connection}:
\begin{itemize}
 \item nell'HTTP non persistente il valore predefinito è \texttt{Close}, perché bisogna chiudere la connessione TCP corrente prima di richiedere l'oggetto successivo;
\item nell'HTTP persistente il valore predefinito è \texttt{Keep-Alive}, perché la connessione va chiusa solo dopo che è stato ricevuto l'ultimo oggetto.
\end{itemize}

\paragraph{Messaggi di risposta}
Il server invia dei \textbf{messaggi di risposta}, nella cui intestazione specifica alcuni campi ASCII tra cui il codice di stato:
\begin{itemize}
 \item \texttt{200 OK}: la richiesta è stata completata con successo, ad esempio:\\
\centerline{\texttt{HTTP/1.1 200 OK}}
\item \texttt{301 Mover Permanently}: l'oggetto richiesto è stato spostato in una nuova posizione;
\item \texttt{400 Bad Request}: il formato del messaggio non è stato riconosciuto;
\item \texttt{404 Not Found}: l'oggetto richiesto non è stato trovato;
\item \texttt{505 HTTP Version Not Supported}: la versione HTTP non è supportata.
\end{itemize}

\subsection{Cookie}
L'HTTP è \textbf{stateless}: il server non conserva alcuna informazione relativa alle passate richieste da parte del client.

Un cookie è una stringa di testo salvato dal browser sull'host client che contiene alcune informazioni relative alla sessione utente (nome utente, lingua personalizzata), in modo che a ogni successiva visita alla stessa sezione del web il client la rimandi al server e quest'ultimo possa ricordare le operazioni svolte in passato dall'utente senza dover ricominciare da zero.

\subsection{Server proxy}
Il traffico di una rete locale può passare attraverso un \textbf{server proxy},\footnote{L'utente deve specificare manualmente al browser l'indirizzo del server proxy.} che è dotato di una cache in cui salvare le ultime risposte HTTP avvenute $\Rightarrow$ si riduce il tempo di risposta nel caso in cui più client collegati al server proxy richiedano lo stesso oggetto, evitando di sovraccaricare il server di origine. Il server proxy, prima di consegnare la copia in cache al client, verifica con una richiesta HEAD al server di origine che l'oggetto non sia stato modificato nel frattempo: l'intestazione del messaggio di risposta contiene il campo \texttt{Last-Modified}, che informa sulla data di ultimo aggiornamento dell'oggetto. Anche il browser dell'utente può avere una cache locale.

\section{Protocollo FTP}
Il \textbf{protocollo File Transfer Protocol} (FTP) è ottimizzato per il trasferimento di file. Usa il TCP. L'host lato server non deve essere una macchina attrezzata appositamente a fare da server, ma può essere una macchina qualunque.

Al contrario dell'HTTP, l'FTP è \textbf{out of band}:\footnote{Si veda la sezione~\ref{sez:segnalazione_canale}.} esiste una connessione dedicata alle \textbf{informazioni di controllo}, e il trasferimento di ogni file richiede l'apertura e la chiusura di una connessione TCP separata dalla connessione di controllo.

Anche nell'FTP i comandi e le risposte sono di tipo ASCII. Tra i comandi:
\begin{itemize}
 \item \texttt{LIST} restituisce l'elenco dei file nel direttorio corrente;
\item \texttt{RETR nome\_file}: recupera un file;
\item \texttt{STOR nome\_file}: archivia un file in un host remoto.
\end{itemize}

\section{Posta elettronica}
\begin{enumerate}
 \item l'\textbf{utente mittente} contatta il suo server di posta e gli invia l'e-mail;
\item l'e-mail, salvata nella mailbox del \textbf{server mittente}, entra in una coda\footnote{Non è una vera e propria coda.} di messaggi da inviare non appena il server mittente riesce a contattare il server destinatario;
\item l'e-mail viene ricevuta dal \textbf{server destinatario} e salvata nella mailbox del server;
\item quando l'\textbf{utente destinatario} accederà alla posta troverà l'e-mail.
\end{enumerate}

\subsection{Protocollo SMTP}
Il \textbf{protocollo Simple Mail Transfer Protocol} (SMTP) serve per l'invio di posta elettronica al server di posta. Utilizza il TCP (tipicamente sulla well-known port 25).

Un \textbf{relay mail server} è un server di posta configurato per accettare la posta proveniente da qualsiasi indirizzo IP, in modo che l'utente titolare dell'account di posta possa accedere alla propria posta da qualsiasi postazione Internet. L'RFC del protocollo SMTP tuttavia sconsiglia i relay server come misura anti-spam; inoltre il contenuto del comando HELO viene spesso confrontato con una blacklist.

\subsubsection{Esempio di conversazione}
I server comunicano tra di loro tramite comandi e risposte testuali ASCII (caratteri ASCI a 7 bit) $\Rightarrow$ vantaggioso per il debug, ma svantaggioso per lo spreco di byte.

\begin{adjustwidth}{\the\parindent}{}
\texttt{\textbf{\textsl{\color{gray}{\# Handshaking (presentazione)}}}}\\
\texttt{C:\ \ \textsl{\textless apre la connessione\textgreater}}\\
\texttt{S:\ \ 220 \textsl{nome\_server\_B}\ \ \ \textsl{\color{gray}{\# la conversazione inizia sempre con il codice 220}}}\\
\texttt{C:\ \ HELO \textsl{nome\_server\_A}\ \ \ \textsl{\color{gray}{\# comunica il proprio nome}}}\\
\texttt{S:\ \ 250\ \ \ \textsl{\color{gray}{\# il nome è stato ricevuto}}}\\
\texttt{C:\ \ MAIL FROM: \textsl{\textless indirizzo\_email\_mittente\textgreater}}\\
\texttt{S:\ \ 250\ \ \ \textsl{\color{gray}{\# conferma del mittente}}}\\
\texttt{C:\ \ RCPT TO: \textsl{\textless indirizzo\_email\_destinatario\textgreater}}\\
\texttt{S:\ \ 250\ \ \ \textsl{\color{gray}{\# conferma del destinatario}}}\\
[\baselineskip]
\texttt{\textbf{\textsl{\color{gray}{\# Trasferimento del messaggio}}}}\\
\texttt{C:\ \ DATA\ \ \ \textsl{\color{gray}{\# voglio iniziare a scrivere il messaggio}}}\\
\texttt{S:\ \ 354\ \ \ \textsl{\color{gray}{\# scrivi pure}}}\\
\texttt{C:\ \ To: \textsl{indirizzo\_email\_destinatario}}\\
\texttt{C:\ \ From: \textsl{indirizzo\_email\_mittente}}\\
\texttt{C:\ \ Subject: \textsl{oggetto\_messaggio}}\\
\texttt{C:\ \ \textsl{\color{gray}{\# una riga vuota, e più precisamente il carattere CRLF, separa}}}\\
\phantom{} \hfill \texttt{\textsl{\color{gray}{l'intestazione dal corpo del messaggio}}}\\
\texttt{C:\ \ \textsl{corpo\_messaggio}}\\
\texttt{C:\ \ .\ \ \ \textsl{\color{gray}{\# il punto sta a significare la fine del messaggio}}}\\
\texttt{S:\ \ 250\ \ \ \textsl{\color{gray}{\# messaggio accettato}}}\\
[\baselineskip]
\texttt{\textbf{\textsl{\color{gray}{\# Chiusura della sessione}}}}\\
\texttt{C:\ \ QUIT\ \ \ \textsl{\color{gray}{\# voglio chiudere la connessione}}}\\
\texttt{S:\ \ 221}\\
\texttt{C:\ \ \textsl{\textless chiude la connessione\textgreater}}
\end{adjustwidth}

Si noti che, poiché le informazioni inviate nella fase di handshaking non vengono mantenute, l'indirizzo del mittente e quello del destinatario vengono riportati anche nel testo stesso del messaggio.

\subsubsection{Allegati}
L'RFC originale del protocollo SMTP prevedeva solamente lo scambio di testo. L'evoluzione di questo RFC permette anche lo scambio di file grazie al \textbf{Multimedia Mail Extension} (MIME), cioè delle righe aggiuntive nell'intestazione del messaggio che dichiarano il tipo di contenuto MIME del file.

\begin{samepage}
\paragraph{Esempio: invio di un'immagine JPEG} \mbox{}

\nopagebreak

\begin{adjustwidth}{\the\parindent}{}
\texttt{...}\\
\texttt{C:\ \ To: \textsl{indirizzo\_email\_destinatario}}\\
\texttt{C:\ \ From: \textsl{indirizzo\_email\_mittente}}\\
\texttt{C:\ \ Subject: \textsl{oggetto\_messaggio}}\\
\texttt{C:\ \ MIME-Version: 1.0}\\
\texttt{C:\ \ Content-Transfer-Encoding: base64}\\
\texttt{C:\ \ Content-Type: image/jpeg}\\
\texttt{C:\ \ }\\
\texttt{C:\ \ \textsl{bit\_immagine}\ \ \ \textsl{\color{gray}{\# corpo del messaggio}}}
\end{adjustwidth}
\end{samepage}

\subsection{Protocollo POP3}
Il \textbf{protocollo Post Office Protocol version 3} (POP3) serve per il recupero della posta elettronica dal server di posta. Usa il TCP.

Il protocollo Internet Mail Access Protocol (IMAP) migliora il protocollo POP3, che è stateless.

\subsubsection{Esempio di conversazione}
\paragraph{Ipotesi}
\begin{itemize}
 \item ci sono 2 messaggi di posta non letti sul server;
\item non si utilizza il protocollo di sicurezza SSL.
\end{itemize}

% \begin{addmargin}[\parindent]{0em}
% \setlength{\parskip}{\baselineskip}
% \begin{minipage}{\dimexpr\textwidth-\the\parindent} % \the\parindent = default value of \parindent = 15pt
\begin{adjustwidth}{\the\parindent}{}
\texttt{\textbf{\textsl{\color{gray}{\# Autorizzazione}}}}\\
\texttt{C:\ \ \textsl{\textless apre la connessione\textgreater}}\\
\texttt{S:\ \ +OK\ \ \ \textsl{\color{gray}{\# server POP3 pronto}}}\\
\texttt{C:\ \ user \textsl{nome\_utente}}\\
\texttt{S:\ \ +OK\ \ \ \textsl{\color{gray}{\# il server può rispondere +OK o \textminus ERR}}}\\
\texttt{C:\ \ pass \textsl{password}\ \ \ \textsl{\color{gray}{\# la password è in chiaro $\Rightarrow$ serve l'SSL}}}\\
\texttt{S:\ \ +OK\ \ \ \textsl{\color{gray}{\# accesso con successo}}}\\
\\
\texttt{\textbf{\textsl{\color{gray}{\# Transazione}}}}\\
\texttt{C:\ \ list\ \ \ \textsl{\color{gray}{\# quanti nuovi messaggi?}}}\\
\texttt{S:\ \ 1 498}\\
\texttt{S:\ \ 2 912}\\
\texttt{...}\\
\texttt{S:\ \ .\ \ \ \textsl{\color{gray}{\# messaggi finiti}}}\\
\\
\texttt{C:\ \ retr 1\ \ \ \textsl{\color{gray}{\# chiede il contenuto del primo messaggio}}}\\
\texttt{S:\ \ \textsl{contenuto\_messaggio}}\\
\texttt{S:\ \ .\ \ \ \textsl{\color{gray}{\# il contenuto del messaggio è terminato}}}\\
\texttt{C:\ \ dele 1\ \ \ \textsl{\color{gray}{\# ordina di eliminare il messaggio dal server}}}\\
\\
\texttt{C:\ \ retr 2\ \ \ \textsl{\color{gray}{\# chiede il contenuto del secondo messaggio}}}\\
\texttt{S:\ \ \textsl{contenuto\_messaggio}}\\
\texttt{S:\ \ .\ \ \ \textsl{\color{gray}{\# il contenuto del messaggio è terminato}}}\\
\texttt{C:\ \ dele 2\ \ \ \textsl{\color{gray}{\# ordina di eliminare il messaggio dal server}}}\\
\\
\texttt{\textbf{\textsl{\color{gray}{\# Chiusura della sessione}}}}\\
\texttt{C:\ \ quit\ \ \ \textsl{\color{gray}{\# voglio chiudere la connessione}}}\\
\texttt{S:\ \ +OK}\\
\texttt{C:\ \ \textsl{\textless chiude la connessione\textgreater}}
\end{adjustwidth}
% \end{minipage}
%\setlength{\parskip}{0pt}

Il comportamento predefinito del client di posta, previsto dall'RFC del POP3, è quello di ordinare l'eliminazione del messaggio subito dopo averlo ricevuto dal server; l'utente può configurare il proprio client di posta affinché il messaggio sia mantenuto sul server.

\section{Reti P2P}
Nelle \textbf{reti P2P} non esiste un server sempre acceso al quale tutti si collegano per ricevere i file: i nodi di una rete P2P, chiamati \textbf{peer}, fungono sia da client sia da server, e sono connessi in modo intermittente.

Più peer stanno condividendo un file, più le prestazioni di distribuzione migliorano, perché un peer mentre sta scaricando il file può nel frattempo condividere la parte scaricata con un altro peer, mentre nel mondo client-server la banda dell'unico server è limitata:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.33\linewidth]{pic/19/Prestazioni_client-server_vs_P2P.png}
\end{figure}

\subsection{Protocollo BitTorrent}
I file sono suddivisi in parti (chunk) da 256 KB. Il \textbf{torrent} è il gruppo di peer che sta condividendo i chunk di un file; la rete P2P è chiamata \textbf{rete overlay}. Il \textbf{tracker} è il server che gestisce la distribuzione di un certo file tra i peer. Un host si collega periodicamente al tracker, il cui indirizzo è specificato in un file .torrent, e il tracker fornisce all'host una lista composta da un numero massimo di indirizzi IP di peer a cui l'host potrà collegarsi per recuperare il file.

Tra i peer si distinguono i \textbf{seed}, cioè gli host che possiedono l'intero file. I seed possono altruisticamente rimanere collegati alla rete oppure egoisticamente abbandonarla: il \textbf{churn}, cioè il tasso di abbandono, influisce molto sulle prestazioni della rete.

L'host può scambiare i chunk del file con i peer che il tracker gli ha comunicato.

\paragraph{Richiesta di chunk}
L'host chiede periodicamente a ogni peer una lista dei chunk che possiedono, quindi applica un algoritmo \textbf{local rarest first}: richiede il chuck mancante più raro nel vicinato, cioè quello che è posseduto da meno peer.

\paragraph{Invio di chunk}
Tra quelli che stanno richiedendo chunk, periodicamente l'host sceglie (choke) i 4 peer da cui è riuscito a scaricare a una velocità più alta nel passato, e inizia a mandare loro dei chunk. Questo meccanismo, chiamato \textbf{tit-for-tat}, serve per svantaggiare i freerider, cioè gli host che scaricano senza condividere. Per evitare di contattare sempre gli stessi peer, si applica l'\textbf{optimistically unchoke}: ogni 30 secondi l'host sceglie in modo casuale un altro peer e inizia a inviargli dei chunk, con la speranza di essere in futuro scelto da quel peer in modo da poter ricevere dei chunk che i soliti peer non hanno.

\subsection{DHT}
Napster, la prima rete P2P, era costituito da un unico tracker che serviva l'intera rete $\Rightarrow$ scollegando il server fu facile smantellare la rete per motivi legali. Le reti P2P si stanno sempre più orientando verso il \textbf{Distributed Hash Table} (DHT): il database P2P, anziché essere concentrato in singoli tracker, è distribuito tra i peer stessi. Il database è organizzato per coppie (\textit{chiave}, \textit{valore}) (ad esempio \textit{chiave} = titolo del film, \textit{valore} = indirizzo IP). Un peer può interrogare il database distribuito facendo una ricerca per chiave, e può anche inserire una coppia (\textit{chiave}, \textit{valore}).

Le coppie (\textit{chiave}, \textit{valore}) vengono assegnate ai peer tramite una tabella di hash:\footnote{Si veda il capitolo ``Le tabelle di hash'' negli appunti di \textit{Algoritmi e programmazione}.}
\begin{enumerate}
\item ogni chiave viene convertita in un id intero da una funzione di hash;
\item ad ogni peer viene assegnato un id intero (per esempio una funzione di hash converte l'indirizzo IP del peer);
\item la coppia viene assegnata al peer il cui id è pari o, nel caso non sia esistente, immediatamente successivo all'id della chiave; se viene superato il massimo id presente nella rete, la coppia viene assegnata al peer di id più basso.
\end{enumerate}

Una rete P2P con DHT è \textbf{strutturata}, cioè il grafo della rete non è costruito in modo casuale.

\subsubsection{Chord}
La rete \textbf{Chord} ha una topologia circolare: ogni peer, oltre a conoscere il proprio id, conosce solo gli indirizzi IP del suo immediato precedessore e del suo immediato successore $\Rightarrow$ la ricerca di una chiave percorre la catena di peer fino ad arrivare al peer il cui id è pari o immediatamente successivo all'id della chiave cercata.

La complessità di questo meccanismo di ricerca è alta ($ O \left( n \right) $) rispetto al sistema con il tracker ($ O \left( 1 \right) $).

Grazie ai \textbf{shortcut}, cioè delle ``scorciatoie'' verso i peer distanti un numero di hop potenza del 2, le prestazioni migliorano a $O \left( \log{n} \right)$.

\subsubsection{Kademlia}
La rete Kademlia di eMule è invece basata su una topologia ad albero binario:\footnote{Si veda il capitolo ``Alberi binari di ricerca (BST)'' negli appunti di \textit{Algoritmi e programmazione}.} ha prestazioni $O \left( \log{n} \right)$ senza dover ricorrere a shortcut.