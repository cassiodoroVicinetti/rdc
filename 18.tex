\chapter[Livello Trasporto: Protocolli TCP-UDP]{Livello Trasporto: Protocolli TCP-UDP\footnote{Questo capitolo include contenuti CC BY-SA da Wikipedia in inglese: \href{https://en.wikipedia.org/wiki/Transmission_Control_Protocol}{Transmission Control Protocol}.}}
\label{cap:livello_trasporto}
Il \textbf{Transmission Control Protocol} (TCP) e lo \textbf{User Datagram Protocol} (UDP) sono due protocolli di livello trasporto.

Ogni host ha un unico indirizzo IP, ma può avere in esecuzione più applicazioni. Ogni applicazione comunica attraverso una porta TCP e una porta UDP; il livello di trasporto effettua poi il \textbf{multiplexing} delle connessioni, cioè gestisce i flussi provenienti dalle varie porte verso il protocollo di rete sottostante.

\section{TCP}
\subsection{Formato dell'intestazione dei pacchetti TCP}
L'intestazione dei pacchetti TCP ha il formato seguente:
\begin{itemize}
 \item \textbf{source port:} la porta TCP sorgente;
\item \textbf{destination port:} la porta TCP di destinazione;
\item \textbf{numero di sequenza};
\item \textbf{acknowledgment number:} la destinazione specifica il numero di sequenza del prossimo pacchetto da ricevere;
\item \textbf{hlen:} la lunghezza dell'intestazione;
\item \textbf{flag:} identificano l'informazione contenuta nel pacchetto:
\begin{itemize}
\item il flag ACK identifica se il pacchetto è un ACK;
\item il flag SYN serve per l'apertura della connessione;
\item il flag FIN serve per la chiusura della connessione;
\end{itemize}
\item \textbf{window size:} la dimensione della finestra di ricezione del mittente del pacchetto, per permettere l'uso dei protocolli a finestra;\footnote{Si veda il capitolo~\ref{cap:protocolli_finestra}.}
\item \textbf{checksum}.
\end{itemize}

\subsubsection{Numero di sequenza}
Il TCP è un protocollo orientato ai byte: il primo pacchetto inizia con un numero casuale, il secondo pacchetto ha quel numero incrementato del numero di byte di cui è costituito il primo pacchetto, e così via.

I buffer del mittente (coda di trasmissione) e della destinazione (coda di ricezione) non sono necessariamente di uguale dimensione $\Rightarrow$ i dati possono venire segmentati in un modo e riassemblati in un altro.

\subsection{Circuito virtuale}
Il TCP è un protocollo connesso: funziona tramite \textbf{circuito virtuale} tra una porta del mittente e una porta del destinatario.

Il circuito virtuale è \textbf{full-duplex}: consente la comunicazione in entrambi i sensi.

Esistono delle porte standard, chiamate \textbf{well-known port}, tramite cui può essere contattato l'applicativo di destinazione: ad ogni well-known port è assegnato un protocollo ben preciso $\Rightarrow$ l'applicativo di destinazione riesce a sapere quale protocollo vuole usare l'applicativo sorgente per la comunicazione. Ad esempio, la porta 80 è riservata alla comunicazione tramite il protocollo HTTP. Siccome la comunicazione è full-duplex, la risposta uscirà dalla porta 80 dell'applicativo di destinazione e giungerà alla porta aperta prima dall'applicativo sorgente per l'invio della richiesta. Le well-known port sono 1024, una piccola parte rispetto a tutte le porte utilizzabili.

\subsubsection{Apertura della connessione}
\label{sez:syn_ack}
\begin{figure}
	\centering
	\includegraphics[width=0.55\linewidth]{pic/18/Apertura_connessione_TCP.png}
	\caption[]{Three-way handshake di TCP.\footnotemark}
\end{figure}
\footnotetext{Questa immagine è derivata da un'immagine su Wikimedia Commons (\href{https://commons.wikimedia.org/wiki/File:Apertura_connessione_TCP.png}{Apertura connessione TCP.png}), realizzata dall'utente \href{https://commons.wikimedia.org/wiki/User:snubcube}{snubcube}, ed è concessa sotto la \href{http://creativecommons.org/licenses/by-sa/3.0/deed.it}{licenza Creative Commons Attribuzione - Condividi allo stesso modo 3.0 Unported}.}

\noindent
Per aprire una connessione, il TCP usa un three-way handshake:\footnote{Si veda la voce \href{https://it.wikipedia.org/wiki/Handshaking}{Handshaking} su Wikipedia in italiano.}
\begin{itemize}
 \item \textbf{SYN:} il client invia un SYN al server, impostando il numero di sequenza del segmento ad un valore casuale $x$;
\item \textbf{SYN-ACK:} il server risponde con un SYN-ACK, impostando il numero di acknowledgment al numero di sequenza ricevuto più uno ($x + 1$), e scegliendo come numero di sequenza del pacchetto un altro numero casuale $y$;
\item \textbf{ACK:} infine il client invia indietro un ACK al server, impostando il numero di sequenza al valore di acknowledgement ricevuto ($x + 1$), e il numero di acknowledgement al numero di sequenza ricevuto più uno ($y + 1$).
\end{itemize}

A questo punto, sia il client sia il server hanno ricevuto un acknowledgment della connessione e la comunicazione full-duplex è stabilita.
\FloatBarrier

\subsubsection{Chiusura della connessione}
\begin{figure}
	\centering
	\includegraphics[width=0.3\linewidth]{pic/18/Chiusura_connessione_TCP.png}
\end{figure}

\noindent
La chiusura della connessione richiede l'invio di FIN e ACK in entrambe le direzioni. È una chiusura di tipo graceful leaving, perché gli interlocutori non abbandonano improvvisamente la conversazione ma prima la chiudono interagendo in maniera "educata".
\FloatBarrier

\subsection{Uso dei protocolli a finestra}
Il TCP richiede più banda dell'UDP, a causa delle maggiori dimensioni dell'header dovute ai maggiori controlli: a differenza dell'UDP, il TCP effettua il \textbf{controllo di flusso} e \textbf{di congestione}, e garantisce che tutti i pacchetti vengano consegnati integri a destinazione. L'acknowledge è obbligatorio (normalmente tramite piggybacking\footnote{Si veda la sezione~\ref{sez:piggybacking}.}), e se un pacchetto non è giunto a destinazione va ritrasmesso.

Se un segmento va perso e arrivano segmenti successivi nella sequenza, il ricevitore inizia ad inviare NACK duplicati riferiti al segmento mancante. Il trasmettitore capisce che deve ritrasmettere il segmento quando riceve 3 NACK duplicati.

La dimensione della finestra di trasmissione viene modificata dinamicamente in funzione della capacità della rete e del ricevitore:
\[
 \text{finestra di trasmissione} \, = \min{\left( \text{finestra di ricezione}, \, \text{finestra di congestione} \right)}
\]

Il trasmettitore conosce la finestra di ricezione grazie al campo ``window size'' nell'intestazione dei pacchetti TCP.

\subsubsection{Finestra di congestione}
\begin{figure}
	\centering
	\includegraphics[width=0.45\linewidth]{pic/18/Finestra_di_congestione_TCP.png}
\end{figure}

\noindent
La \textbf{finestra di congestione} serve per evitare di congestionare la rete limitando la finestra di trasmissione. All'apertura della connessione la finestra di congestione ha dimensione pari a 1, cioè viene mandato un solo pacchetto. Man mano che arrivano gli ACK, la finestra di congestione:
\begin{itemize}
 \item cresce esponenzialmente (2, 4, 8\dots) fino ad arrivare a un certo \textbf{valore di soglia}, che la prima volta è predefinito;
\item superato il valore di soglia cresce linearmente.
\end{itemize}

Se un riscontro non arriva entro il timeout, il valore di soglia viene impostato alla metà del valore corrente della finestra di congestione e si riparte da 1.
\FloatBarrier

\section{UDP}
Il protocollo UDP è privo dei controlli di flusso e di congestione $\Rightarrow$ il trasmettitore continua a trasmettere alla massima velocità consentita dalla banda di trasmissione senza aspettare alcun ACK. Esegue solamente il controllo degli errori, cioè verifica il checksum del pacchetto per verificarne l'integrità.

L'UDP è un protocollo non connesso:
\begin{itemize}
 \item non vi è alcun ritardo di apertura e di chiusura della connessione;
\item è basso l'overhead, cioè il carico sul sistema operativo per tenere aperte le connessioni;
\item i ritardi sono bassi e costanti, cioè la distanza tra un pacchetto e l'altro è uguale per tutti i pacchetti.
\end{itemize}

L'intestazione dei pacchetti UDP è molto più breve e semplice:
\begin{itemize}
\item source port;
\item destination port;
\item UDP length;
\item UDP checksum.
\end{itemize}

L'UDP è utile:
\begin{itemize}
 \item quando la rete è affidabile $\Rightarrow$ i controlli non servono;
\item quando le prestazioni e la riduzione del ritardo sono più importanti dell'affidabilità (es. telefonia);
\item quando l'applicazione mette tutti i dati in un singolo pacchetto $\Rightarrow$ non servono meccanismi per regolare la dimensione della finestra.
\end{itemize}

Eventuali meccanismi di ritrasmissione, come l'invio di ACK, devono essere gestiti a livello applicazione.