\chapter{Reti locali}
Una rete si dice \textbf{locale} se è diffusa su una piccola estensione geografica. Le prime reti locali sono nate con una topologia a bus, ma oggi si preferisce la topologia a stella.

\section{Protocolli per reti locali}
In una rete locale ad accesso multiplo,\footnote{Si veda la sezione~\ref{sez:condivisione_canale}.} il mezzo trasmissivo è condiviso $\Rightarrow$ può trasmettere solo un nodo alla volta:
\begin{itemize}
 \item \ul{vantaggio:} un nodo quando trasmette ha a disposizione tutta la rete $\Rightarrow$ massima velocità;
\item \ul{vantaggio:} comodo per il traffico broadcast e multicast, dove un nodo comunica con tutti gli altri;
\item \ul{svantaggio:} se un nodo vuole comunicare con un altro nodo specifico, deve inserire l'indirizzo del destinatario.
\end{itemize}

\paragraph{Parametri}
\begin{itemize}
 \item capacità e traffico smaltito (throughput);
\item equità tra gli interlocutori;
\item ritardi di accesso, propagazione, consegna;
\item numero di stazioni, lunghezza della rete, ecc.
\end{itemize}

\paragraph{Classificazione}
\begin{itemize}
 \item \textbf{ad accesso casuale};
\item \textbf{ad accesso ordinato:} si basa sul passaggio di ``testimoni'' detti token $ \Rightarrow$ non ha avuto successo;
\item \textbf{a slot con prenotazione:} chi vuole parlare deve aspettare che gli si venga data la risorsa.
\end{itemize}

\subsection{Protocolli ad accesso casuale}
In un \textbf{protocollo ad accesso casuale}, ogni nodo che vuole trasmettere trasmette quando è necessario, e non c'è un determinismo:
\begin{itemize}
 \item usa la massima velocità permessa dal canale;
\item non si coordina con gli altri nodi.
\end{itemize}

Si ha una \textbf{collisione} quando due o più nodi trasmettono contemporaneamente.

Un \textbf{dominio di collisione} è un insieme di nodi (schede di rete) che concorrono per accedere allo stesso mezzo trasmissivo $\Rightarrow$ la trasmissione contemporanea provocherebbe collisione.

I protocolli ad accesso casuale specificano:
\begin{itemize}
 \item come riconoscere una collisione;
\item come recuperare una collisione (ritrasmissione).
\end{itemize}

\section{Slotted ALOHA}
\begin{figure}[H]
	\centering
	\includegraphics[width=0.66\linewidth]{pic/06/Esempio_Slotted_ALOHA.png}
\end{figure}

Il tempo è suddiviso in \textbf{slot}, e ogni nodo spezza la conversazione in slot. Quando un nodo si accorge che c'è stata una collisione in un certo slot (per esempio a causa della mancanza di un ACK), ritenta la trasmissione in uno slot scelto a caso, finché a forza di tentare la trasmissione non va a buon fine:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.25\linewidth]{pic/06/Diagramma_di_flusso_Slotted_ALOHA.png}
\end{figure}

È fondamentale che lo slot sia scelto a caso, perché se tutti i collidenti ritrasmettessero aspettando tutti lo stesso numero di slot le collisioni sarebbero senza fine. Il costo di questo coordinamento distribuito è l'inefficienza: ci possono essere degli slot in cui nessuno trasmette.

\section{Pure ALOHA}
\begin{figure}
	\centering
	\includegraphics[width=0.33\linewidth]{pic/06/Pure_ALOHA_example.png}
\end{figure}

\noindent
Non c'è la sincronizzazione tra gli interlocutori, ovvero non esistono degli slot di tempo ma ognuno può iniziare a comunicare in qualunque istante. Il problema è che aumentano le collisioni: se l'interlocutore A inizia a trasmettere quando l'interlocutore B deve ancora finire, tutto il pacchetto trasmesso da B dovrà essere buttato via.

\paragraph{Svantaggio}
\begin{itemize}
 \item le prestazioni non sono molto alte: sotto ipotesi di traffico uniforme, il throughput massimo è pari al 37\% nello Slotted ALOHA, e al 18\% nel Pure ALOHA;
\item il canale si intasa prima nel Pure ALOHA, perché aumentano molto le collisioni con il carico;
\item i ritardi di accesso non sono controllabili a priori in modo deterministico.
\end{itemize}

\paragraph{Vantaggi}
\begin{itemize}
 \item protocolli semplici;
\item a basso carico, il ritardo di accesso è nullo o contenuto.
\end{itemize}
\FloatBarrier

\section{CSMA}
Il \textbf{Carrier Sense Multiple Access} (CSMA) prevede l'ascolto del canale (CS) prima della trasmissione:
\begin{itemize}
 \item se sente che il canale è libero: il nodo trasmette il pacchetto;
\item se sente che il canale è occupato:
\begin{itemize}
 \item CSMA 1-persistente: il nodo continua a verificare se il canale è libero e trasmette appena si libera:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.33\linewidth]{pic/06/Diagramma_di_flusso_CSMA_1-persistente.png}
\end{figure}
\item CSMA 0-persistente: il nodo riprova dopo un tempo casuale:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.33\linewidth]{pic/06/Diagramma_di_flusso_CSMA_0-persistente.png}
\end{figure}
\item CSMA $p$-persistente: il nodo con probabilità $1 - p$ aspetta un tempo casuale (0-persistente), con probabilità $p$ riverifica subito (1-persistente).
\end{itemize}
\end{itemize}

Questo protocollo è più efficiente in termini di thoughput. Si possono però verificare ugualmente delle collisioni:
\begin{itemize}
 \item due interlocutori possono decidere nello stesso istante di iniziare a trasmettere $ \Rightarrow $ si ha una \textbf{sincronizzazione delle collisioni}:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.33\linewidth]{pic/06/CSMA_collision_synchronization.png}
\end{figure}
\item se si tiene conto dei tempi di propagazione, un nodo lontano può sentire il canale libero, anche se in realtà è occupato ma la trasmissione non ha ancora raggiunto il nodo lontano $\Rightarrow$ si dice \textbf{intervallo di vulnerabilità} l'intervallo di tempo in cui l'avvio di una trasmissione da parte del nodo lontano creerebbe una collisione (è pari al ritardo di propagazione sul canale), e questo intervallo è tanto grande quando la distanza è maggiore $\Rightarrow$ questo protocollo funziona bene su reti piccole:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.33\linewidth]{pic/06/Intervallo_di_vulnerabilita_CSMA.png}
\end{figure}
\end{itemize}

Pertanto il CS non risolve del tutto il problema delle collisioni.

\section{CSMA/CD}
Nel \textbf{Carrier Sense Multiple Access/Collision Detect} (CSMA/CD), anziché trasmettere l'intero pacchetto e soltanto alla fine verificare la collisione, il nodo durante la trasmissione ogni tanto cerca di capire se si è verificata una collisione, e in caso affermativo interrompe subito la trasmissione, evitando di sprecare il canale per una trasmissione inutile.

Nella comunicazione via radio non è possibile implementare la collision detection, perché è difficile per un interlocutore capire se altri stanno parlando in quel momento: in trasmissione non conviene ascoltare il canale, e in ricezione non conviene trasmettere, perché la ricezione sarebbe disturbata dalla trasmissione.

Siccome l'accesso alla rete è conteso, quando si riesce a ottenere l'accesso alla rete conviene trasmettere pacchetti grandi. Esiste un vincolo tra la dimensione minima del pacchetto e la dimensione della rete per riconoscere le collisioni: si veda la sezione~\ref{sez:dimensione_minima_pdu}.

Ethernet implementa il CSMA/CD 1-persistente perché è pensato per reti scariche.

Il \textbf{backoff} è esponenziale nelle ritrasmissioni:
\begin{itemize}
 \item 1\textsuperscript{a} ritrasmissione: se c'è una collisione il nodo aspetta un tempo scelto a caso tra 0 e 1;
\item 2\textsuperscript{a} ritrasmissione: se c'è una collisione il nodo aspetta un tempo scelto a caso da 0 a 3;
\item 3\textsuperscript{a} ritrasmissione: se c'è una collisione il nodo aspetta un tempo scelto a caso da 0 a 7;
\end{itemize}
e così via.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\linewidth]{pic/06/Prestazioni_protocolli_ad_accesso_casuale.png}
	\caption{Prestazioni dei protocolli ad accesso casuale.}
\end{figure}